# Demo app with Gulp, React and Mocha #

## Reports generated with mochawesome ##
## This is a trivia app with gulp browserify, browserSync and react
This configuration it's included to make a base project with gulp and react ES5. ##

**Install dependencies
**
npm install

**Run app
**
-- gulp

**Run test
**
-- npm test

**Find reports in test-reports folder**
