var babel = require('babel-core/register');
var babelify = require('babelify');
var browserify = require('browserify');
var browserSync = require('browser-sync').create();
var gulp = require('gulp');
var concat = require('gulp-concat');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var source = require('vinyl-source-stream');
var watchify = require('watchify');
var mocha = require('gulp-mocha');
var title = 'demo test';
var fs = require('fs');

// compiler for react tests
require('./compiler');

var dependencies = [
    'react',
    'react-dom'
];

function bundle (bundler) {
    return bundler
        .transform('babelify', {
            presets: ['es2015', 'react']
        })
        .bundle()
            .on('error', gutil.log)
            .pipe(source('bundle.js'))
            .pipe(gulp.dest('./web/js/'))
            .pipe(browserSync.stream());
}

gulp.task('watch', function () {
    var watcher = watchify(browserify('./app/app.js', watchify.args));
    bundle(watcher);

    watcher.on('update', function () {
        bundle(watcher);
    });

    watcher.on('log', gutil.log);

    browserSync.init({
        port: '8081',
        server:  {
            baseDir: './'
        },
        logFileChanges: false
    });
});

gulp.task('mocha', function() {
    return gulp
        .src('app/**/__tests__/*.js', {
            read: false
        })
        .pipe(mocha({
            compilers: {
                js: babel
            },
        reporter: 'mochawesome',
        reporterOptions: {
            autoOpen: true,
            reportDir: 'test-reports',
            reportFilename: 'test',
            reportTitle: title,
            reportPageTitle: title,
            enableCharts: false,
            enableCode: true
        }
    }));
});

gulp.task('js', function () {
    return bundle(browserify('./app/app.js'));
});

gulp.task('styles', function () {
    return gulp.src('./app/**/*.scss')
        .pipe(sass())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./web/css/'));
});

gulp.task('default', ['watch', 'styles']);
