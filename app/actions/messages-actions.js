var AppActions = require('./app-actions');
var AppDispatcher = require('../dispatcher/app-dispatcher');
var MessagesService = require('../services/messages-service');

var MessagesActions = {

    getMessage: function (data) {
        MessagesService.get(data, function (err, response) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_MESSAGE',
                    data: response.message
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    },

    getMessages: function () {
        MessagesService.getAll(function(err, response) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_MESSAGES',
                    data: response.messages
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    }
};

module.exports = MessagesActions;
