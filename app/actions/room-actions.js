var AppDispatcher = require('../dispatcher/app-dispatcher');

var RoomActions = {

    getRoom: function(data) {
        AppDispatcher.handleViewAction({
            actionType: 'GET_ROOM',
            data: data
        });
    },

    getRooms: function (data) {
        AppDispatcher.handleViewAction({
            actionType: 'GET_ROOMS',
            data: data
        });
    },

    updateRooms: function (data) {
        AppDispatcher.handleViewAction({
            actionType: 'UPDATE_ROOMS',
            data: data
        });
    }
};

module.exports = RoomActions;
