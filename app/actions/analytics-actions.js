var AppActions = require('./app-actions');
var AppDispatcher = require('../dispatcher/app-dispatcher');
var AnalyticsService = require('../services/analytics-service');

var AnalyticsActions = {
    
    getAnalytics: function () {
        AppActions.setLoading({
            type: 'analytics',
            value: true
        });

        AnalyticsService.getAll(function(error, response) {
            if (!error) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_ANALYTICS',
                    data: response.data
                });
            } else {
                AppActions.setError({ code: error.error.code });
            }

            AppActions.setLoading({
                type: 'analytics',
                value: false
            });
        })
    }
};

module.exports = AnalyticsActions;