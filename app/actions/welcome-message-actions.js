var AppDispatcher = require('../dispatcher/app-dispatcher');
var WelcomeMessagesService = require('../services/welcome-messages-service');

var WelcomeMessageActions = {

	createMessage: function (message) {
		AppDispatcher.handleViewAction({
			actionType: 'CREATE_WELCOME_MESSAGE',
			data: message
		})
	},

	getMessages: function () {
		WelcomeMessagesService.getAll(function (err, response) {
			if (!err) {
				AppDispatcher.handleViewAction({
					actionType: 'GET_WELCOME_MESSAGE',
					messages: response
				})
			}
		});
	},

	setMessage: function (messages) {
		AppDispatcher.handleViewAction({
			actionType: 'SET_WELCOME_MESSAGE',
			data: messages
		})
	},

	deleteMessage: function (message) {
		AppDispatcher.handleViewAction({
			actionType: 'DELETE_WELCOME_MESSAGE',
			data: message
		})
	}
}

module.exports = WelcomeMessageActions;
