var AppActions = require('./app-actions');
var AppDispatcher = require('../dispatcher/app-dispatcher');
var GroupsService = require('../services/groups-service');

var GroupsActions = {

    getGroup: function (id) {
        AppActions.setLoading({
            type: 'group',
            value: true
        });

        GroupsService.get(id, function(err, response) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_GROUP',
                    data: response.group
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }

            AppActions.setLoading({
                type: 'group',
                value: false
            });
        });
    },

    getGroups: function () {
        AppActions.setLoading({
            type: 'groups',
            value: true
        });

        GroupsService.getAll(function(err, response) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_GROUPS',
                    data: response.groups
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }

            AppActions.setLoading({
                type: 'groups',
                value: false
            });
        });
    }
};

module.exports = GroupsActions;
