var AppActions = require('./app-actions');
var AppDispatcher = require('../dispatcher/app-dispatcher');
var FeedbackService = require('../services/feedback-service');

var FeedbackActions = {

    getFeedback: function (id) {
        AppActions.setLoading({
            type: 'feedback',
            value: true
        });

        FeedbackService.get(id, function(err, response) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_FEEDBACK',
                    data: response.feedback
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }

            AppActions.setLoading({
                type: 'feedback',
                value: false
            });
        });
    },

    getFeedbacks: function () {
        AppActions.setLoading({
            type: 'feedback',
            value: true
        });

        FeedbackService.getAll(function(err, response) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_FEEDBACKS',
                    data: response.feedbacks
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }

            AppActions.setLoading({
                type: 'feedback',
                value: false
            });
        });
    }
};

module.exports = FeedbackActions;
