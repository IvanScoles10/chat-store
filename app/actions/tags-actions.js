var AppActions = require('./app-actions');
var AppDispatcher = require('../dispatcher/app-dispatcher');
var TagsService = require('../services/tags-service');

var TagsActions = {

    getTag: function (data) {
        TagsService.get(data, function (err, response) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_TAG',
                    data: response.tag
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    },

    getTags: function () {
        AppActions.setLoading({
            type: 'tags',
            value: true
        });

        TagsService.getAll(function(err, response) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_TAGS',
                    data: response.tags
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }

            AppActions.setLoading({
                type: 'tags',
                value: false
            });
        });
    }
};

module.exports = TagsActions;
