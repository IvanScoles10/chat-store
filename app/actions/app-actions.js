var AppDispatcher = require('../dispatcher/app-dispatcher');

var AppActions = {

    setLoading: function (data) {
        AppDispatcher.handleViewAction({
            actionType: 'SET_LOADING',
            data: data
        });
    },

    setError: function (data) {
        AppDispatcher.handleViewAction({
            actionType: 'SET_ERROR',
            data: data
        });
    }
};

module.exports = AppActions;
