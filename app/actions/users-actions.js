var AppActions = require('./app-actions');
var AppDispatcher = require('../dispatcher/app-dispatcher');
var UsersService = require('../services/users-service');

var UsersActions = {

    getUser: function (data) {
        AppDispatcher.handleViewAction({
            actionType: 'GET_USER',
            data: data
        });
    },

    getUsers: function () {
        AppActions.setLoading({
            type: 'users',
            value: true
        });

        UsersService.getAll(function (err, users) {
            if (!err) {
                AppDispatcher.handleViewAction({
                    actionType: 'GET_USERS',
                    data: users
                });
            } else {
                AppActions.setError({ code: err.error.code });
            }

            AppActions.setLoading({
                type: 'users',
                value: false
            });
        });
    },

    setUser: function (data) {
        AppDispatcher.handleViewAction({
            actionType: 'SET_USER',
            data: data
        });
    },

    removeUser: function (data) {
        AppDispatcher.handleViewAction({
            actionType: 'REMOVE_USER',
            data: data
        });
    }
};

module.exports = UsersActions;
