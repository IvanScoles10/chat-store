var _ = require('lodash');
var request = require('request');
var PATH = 'https://chat-store-node-server.herokuapp.com/api/v1';
//PATH = 'http://127.0.0.1:3000/api/v1';
var PATH_GET_MESSAGES;
var PATH_CREATE_MESSAGES;
var PATH_GET_MESSAGES_BY_ID;
var PATH_SET_MESSAGE_STATUS;
var PATH_SET_MESSAGE_TAGS;
var PATH_SET_MESSAGE_GROUP;
var PATH_SET_MESSAGE_OWNER;

const MessagesService = {

    getAll: function (cb) {
        PATH_GET_MESSAGES = PATH + '/messages';

        request.get({
            url: PATH_GET_MESSAGES
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    create: function (data, cb) {
        PATH_CREATE_MESSAGES = PATH + '/messages';

        request.post({
            url: PATH_CREATE_MESSAGES,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    get: function (id, cb) {
        PATH_GET_MESSAGES_BY_ID = PATH + '/messages/' + id;

        request.get({
            url: PATH_GET_MESSAGES_BY_ID
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    setStatus: function (data, cb) {
        PATH_SET_MESSAGE_STATUS = PATH + '/messages/' + data.id  + '/status/' + data.status;

        request.post({
            url: PATH_SET_MESSAGE_STATUS
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    setTag: function (data, cb) {
        PATH_SET_MESSAGE_TAGS = PATH + '/messages/' + data.id  + '/tags';

        request.post({
            url: PATH_SET_MESSAGE_TAGS,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tag: _.pick(data.tag, [
                    '_id',
                    'name',
                    'description'
                ])
            })
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    setGroup: function (data, cb) {
        PATH_SET_MESSAGE_GROUP = PATH + '/messages/' + data.id  + '/group';

        request.post({
            url: PATH_SET_MESSAGE_GROUP,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                group: _.pick(data.group, [
                    '_id',
                    'name'
                ])
            })
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    setOwner: function (data, cb) {
        PATH_SET_MESSAGE_OWNER = PATH + '/messages/' + data.id  + '/owner';

        request.post({
            url: PATH_SET_MESSAGE_OWNER,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                owner: data.owner
            })
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    }
};

module.exports = MessagesService;
