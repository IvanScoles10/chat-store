var request = require('request');
var PATH = 'https://chat-store-node-server.herokuapp.com/api/v1';
//PATH = 'http://127.0.0.1:3000/api/v1';
var PATH_GET_GROUP;
var PATH_GET_GROUPS;
var PATH_CREATE_GROUP;
var PATH_DELETE_GROUP;
var PATH_UPDATE_USERS_FROM_GROUP;
var PATH_DELETE_USERS_FROM_GROUP;

const GroupsService = {

    getAll: function (cb) {
        PATH_GET_GROUPS = PATH + '/groups';

        request.get({
            url: PATH_GET_GROUPS
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    get: function (id, cb) {
        PATH_GET_GROUP = PATH + '/groups/' + id;

        request.get({
            url: PATH_GET_GROUP
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    create: function (data, cb) {
        PATH_CREATE_GROUP = PATH + '/groups';

        request.post({
            url: PATH_CREATE_GROUP,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    delete: function (id, cb) {
        PATH_DELETE_GROUP = PATH + '/groups/' + id;

        request.delete({
            url: PATH_DELETE_GROUP,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    updateUsers: function (data, cb) {
        PATH_UPDATE_USERS_FROM_GROUP = PATH + '/groups/' + data.id;

        request.put({
            url: PATH_UPDATE_USERS_FROM_GROUP,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    deleteUsers: function (data, cb) {
        PATH_DELETE_USERS_FROM_GROUP = PATH + '/groups/' + data.id + '/' + data.userId;

        request.delete({
            url: PATH_DELETE_USERS_FROM_GROUP,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    }
};

module.exports = GroupsService;
