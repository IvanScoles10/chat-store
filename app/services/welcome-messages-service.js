var request = require('request');
var PATH = 'https://chat-store-node-server.herokuapp.com/api/v1';
//var PATH = 'http://127.0.0.1:3000/api/v1';
var PATH_GET_WELCOME_MESSAGE;
var PATH_CREATE_WELCOME_MESSAGE;
var PATH_UPDATE_WELCOME_MESSAGE;
var PATH_DELETE_WELCOME_MESSAGE;

const welcomeMessagesService = {

    getAll: function (cb) {
        PATH_GET_WELCOME_MESSAGE = PATH + '/welcome-messages';

        request.get({
            url: PATH_GET_WELCOME_MESSAGE
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    create: function (data, cb) {
        PATH_CREATE_WELCOME_MESSAGE = PATH + '/welcome-messages';

        request.post({
            url: PATH_CREATE_WELCOME_MESSAGE,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    update: function (data, cb) {
        PATH_UPDATE_WELCOME_MESSAGE = PATH + '/welcome-messages';

        request({
            method: 'PUT',
            url: PATH_UPDATE_WELCOME_MESSAGE,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    delete: function (id, cb) {
        PATH_DELETE_WELCOME_MESSAGE = PATH + '/welcome-messages';

        request.delete({
            url: PATH_CREATE_WELCOME_MESSAGE,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                _id: id
            })
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    }
};

module.exports = welcomeMessagesService;
