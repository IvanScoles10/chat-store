var request = require('request');
var PATH = 'https://chat-store-node-server.herokuapp.com/api/v1';
//PATH = 'http://127.0.0.1:3000/api/v1';
var PATH_GET_ANALYTICS;

const AnalyticsService = {
	getAll: function (cb) {
		PATH_GET_ANALYTICS = PATH + '/analytics';

		request.get({
			url: PATH_GET_ANALYTICS
		}, function (error, response, body) {
			body = JSON.parse(body);

			if (!body.error) {
				return cb(null, body);
			} else {
				return cb(body);
			}
		});
	}
};

module.exports = AnalyticsService;
