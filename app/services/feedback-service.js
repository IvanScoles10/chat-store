var request = require('request');
var PATH = 'https://chat-store-node-server.herokuapp.com/api/v1';
//PATH = 'http://127.0.0.1:3000/api/v1';
var PATH_GET_FEEDBACK;
var PATH_GET_FEEDBACKS;
var PATH_CREATE_FEEDBACK;
var PATH_DELETE_FEEDBACK;

const FeedbackService = {

    getAll: function (cb) {
        PATH_GET_FEEDBACKS = PATH + '/feedback';

        request.get({
            url: PATH_GET_FEEDBACKS
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    get: function (id, cb) {
        PATH_GET_FEEDBACK = PATH + '/feedback/' + id;

        request.get({
            url: PATH_GET_FEEDBACK
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    create: function (data, cb) {
        PATH_CREATE_FEEDBACK = PATH + '/feedback';

        request.post({
            url: PATH_CREATE_FEEDBACK,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    delete: function (id, cb) {
        PATH_DELETE_FEEDBACK = PATH + '/feedback/' + id;

        request.delete({
            url: PATH_DELETE_FEEDBACK,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    }
};

module.exports = FeedbackService;
