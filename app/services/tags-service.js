var request = require('request');
var PATH = 'https://chat-store-node-server.herokuapp.com/api/v1';
//PATH = 'http://127.0.0.1:3000/api/v1';
var PATH_GET_TAG;
var PATH_GET_TAGS;
var PATH_CREATE_TAG;
var PATH_DELETE_TAG;

const TagsService = {

    getAll: function (cb) {
        PATH_GET_TAGS = PATH + '/tags';

        request.get({
            url: PATH_GET_TAGS
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    get: function (id, cb) {
        PATH_GET_TAG = PATH + '/tags/' + id;

        request.get({
            url: PATH_GET_TAG
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    create: function (data, cb) {
        PATH_CREATE_TAG = PATH + '/tags';

        request.post({
            url: PATH_CREATE_TAG,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    delete: function (id, cb) {
        PATH_DELETE_TAG = PATH + '/tags/' + id;

        request.delete({
            url: PATH_DELETE_TAG,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    }
};

module.exports = TagsService;
