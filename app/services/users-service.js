var request = require('request');
var PATH = 'https://chat-store-node-server.herokuapp.com/api/v1';
//var PATH = 'http://127.0.0.1:3000/api/v1';
var PATH_GET_USERS;
var PATH_CREATE_USER;
var PATH_DELETE_USER;

const UsersService = {

    getAll: function (cb) {
        PATH_GET_USERS = PATH + '/users';

        request.get({
            url: PATH_GET_USERS
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    create: function (data, cb) {
        PATH_CREATE_USER = PATH + '/users';

        request.post({
            url: PATH_CREATE_USER,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: data.email,
                password: data.password,
                username: data.username,
                user_metadata: data.user_metadata
            })
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    },

    delete: function (id, cb) {
        PATH_DELETE_USER = PATH + '/users/' + id;

        request.delete({
            url: PATH_DELETE_USER,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, body) {
            body = JSON.parse(body);

            if (!body.error) {
                return cb(null, body);
            } else {
                return cb(body);
            }
        });
    }
};

module.exports = UsersService;
