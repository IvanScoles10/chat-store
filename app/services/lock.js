var AppActions = require('../actions/app-actions');
var UsersActions = require('../actions/users-actions');
var hash = window.location.hash;

const lock = new Auth0Lock(
    'PODfRLiQn8YMjw3epnD5-GahiQPioQnE',
    'ivanscoles.auth0.com'
);

lock.on('authenticated', function(authResult) {
    AppActions.setLoading({
        type: 'authenticated',
        value: true
    });

    lock.getUserInfo(authResult.accessToken, function(error, profile) {
        if (error) {
            return;
        }

        var user = {
            profile: profile,
            access_token: authResult.accessToken
        };

        UsersActions.setUser(user);
        AppActions.setLoading({
            type: 'authenticated',
            value: false
        });

        if (hash.search('access_token') > 0) {
            hash = hash.replace('/#/dashboard', '');
            window.location = '/#/dashboard';
        }
    });
});


module.exports = lock;
