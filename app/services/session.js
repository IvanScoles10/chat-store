var _ = require('lodash');

const session = {

    name: 'User-v1',

    setUser: function (user) {
        localStorage.setItem(this.name, JSON.stringify(user));
    },

    getUser: function () {
        var user = (localStorage.getItem(this.name) !== 'undefined') ?
            JSON.parse(localStorage.getItem(this.name)) : {};

        return user;
    },

    removeUser: function () {
        localStorage.removeItem(this.name);
    },

    isAuthenticated: function () {
        var user = (localStorage.getItem(this.name) !== 'undefined') ?
            JSON.parse(localStorage.getItem(this.name)) : {};

        return _.get(user, 'access_token');
    },

    setProfile: function (profile) {},

    getProfile: function () {},

    getAccessToken: function () {},

    setAccessToken: function () {}
}

module.exports = session;
