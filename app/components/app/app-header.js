var createReactClass = require('create-react-class');
var Navbar = require('react-bootstrap/lib/Navbar');
var Nav = require('react-bootstrap/lib/Nav');
var NavItem = require('react-bootstrap/lib/NavItem');
var PropTypes = require('prop-types');
var React = require('react');
var Router = require('react-router');
var Link = require('react-router').Link;
var Session = require('../../services/session');
var UsersStore = require('../../stores/users-store');
var UsersActions = require('../../actions/users-actions');

function getUserState() {
    return {
        user: UsersStore.getUser()
    }
}

var AppHeader = createReactClass({

    propTypes: {
        lock: PropTypes.object.isRequired
    },

    componentDidMount: function () {
        UsersStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        UsersStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getUserState());
    },

    render: function () {
        return (
            <header className="app-header navbar">
                <button className="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
                <a className="navbar-brand" href="#"></a>
                <ul className="nav navbar-nav d-md-down-none">
                    <li className="nav-item">
                        <a className="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
                    </li>
                    <li className="nav-item">
                        {this.renderLink('login')}
                        {this.renderLink('logout')}
                        {this.renderLink('contact')}
                    </li>
                </ul>
            </header>
        );
    },

    renderLoginLink: function () {
        return (
            <Link {...this.getProps('login')}>Login</Link>
        );
    },

    renderLogoutLink: function () {
        return (
            <Link {...this.getProps('logout')}>Logout</Link>
        );
    },

    renderContactLink: function () {
        return (
            <Link to="contact">contact</Link>
        );
    },

    renderLink: function (type) {
        var dataToRender = {
            login: this.renderLoginLink(),
            logout: this.renderLogoutLink(),
            contact: this.renderContactLink()
        };

        if (UsersStore.isAuthenticated()) {
            dataToRender['contact'] = null;
            dataToRender['login'] = null;
        } else {
            dataToRender['logout'] = null;
        }

        return dataToRender[type];
    },

    getProps: function (type) {
        return {
            className: 'nav-link',
            onClick: (type === 'login') ? this.showLock : this.logout
        };
    },

    showLock: function () {
        this.props.lock.show({
            allowedConnections: ['Username-Password-Authentication'],
            allowSignUp: false
        });
    },

    logout: function () {
        UsersActions.removeUser();
    }
});

module.exports = AppHeader;
