var AppStore = require('../../stores/app-store');
var createReactClass = require('create-react-class');
var React = require('react');
var Session = require('../../services/session');

function getLoadingState() {
    return {
        loading: AppStore.getLoading('authenticated')
    }
}

const AppLoading = createReactClass({

    getInitialState: function () {
        return getLoadingState();
    },

    componentDidMount: function () {
        AppStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        AppStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getLoadingState());
    },

    render: function () {
        var dataToRender = null;

        if (this.state.loading) {
            dataToRender = (
                <div className="app-loading">
                    <div className="app-loading--text">LOADING</div>
                    <div className="app-loading--content"></div>
                </div>
            );
        }

        return dataToRender;
    }
});

module.exports = AppLoading;
