var createReactClass = require('create-react-class');
var React = require('react');

const AppFooter = createReactClass({

    render: function () {
        return (<footer className="app-footer"></footer>);
    }
});

module.exports = AppFooter;
