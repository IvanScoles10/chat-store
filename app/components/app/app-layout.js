var createReactClass = require('create-react-class');
var AppAlert = require('./app-alert');
var AppFooter = require('./app-footer');
var AppHeader = require('./app-header');
var AppSidebar = require('./app-sidebar');
var React = require('react');

const AppLayout = createReactClass({

    render: function () {
        return (
            <div className="app">
                <AppHeader lock={this.props.route.lock}></AppHeader>
                <div className="app-body">
                    <AppSidebar />
                    <main className="main">
                        <ol className="breadcrumb">..</ol>
                        <div className="container">
                            <div className="wrap-container">
                                <AppAlert />
                            </div>
                        </div>
                        {this.props.children}
                    </main>
                </div>
                <AppFooter />
            </div>
        );
    }
});

module.exports = AppLayout;
