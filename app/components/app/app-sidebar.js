var _ = require('lodash');
var createReactClass = require('create-react-class');
var React = require('react');
var Session = require('../../services/session');
var UsersStore = require('../../stores/users-store');
var Link = require('react-router').Link;

function getUserState() {
    return {
        user: UsersStore.getUser()
    }
};

const AppSidebar = createReactClass({

    getInitialState: function () {
        return getUserState();
    },

    componentDidMount: function () {
        UsersStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        UsersStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getUserState());
    },

    render: function () {
        return (
            <div className="sidebar">
                <nav className="sidebar-nav">
                    <ul className="nav">
                        <li className="nav-item">
                            {this.renderLink('dashboard')}
                            {this.renderLink('feedback')}
                            {this.renderLink('tags')}
                            {this.renderLink('users')}
                            {this.renderLink('groups')}
                            {this.renderLink('analytics')}
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="...">...</a>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    },

    renderDashboardLink: function () {
        return (
            <Link className="nav-link" to="/dashboard" activeClassName="active">
                <i className="fa fa-cog"></i> Dashboard
            </Link>
        );
    },

    renderFeedbackLink: function () {
        return (
            <Link className="nav-link" to="/feedback" activeClassName="active">
                <i className="fa fa-feed"></i> Feedback
            </Link>
        );
    },

    renderTagsLink: function () {
        return (
            <Link className="nav-link" to="/tags" activeClassName="active">
                <i className="fa fa-tags"></i> Tags
            </Link>
        );
    },

    renderAnalytics: function () {
        return (
            <Link className="nav-link" to="/analytics" activeClassName="active">
                <i className="icon-chart"></i> Analytics
            </Link>
        );
    },

    renderGroupsLink: function () {
        return (
            <Link className="nav-link" to="/groups" activeClassName="active">
                <i className="icon-people"></i> Groups
            </Link>
        );
    },

    renderUsersLink: function () {
        var dataToRender = null;
        var profile = _.get(this.state.user, 'profile');
        var userMedata = _.get(profile, 'user_metadata');

        if (userMedata && userMedata.role === 'admin' || userMedata && userMedata.role === 'super_admin') {
            dataToRender = (
                <Link className="nav-link" to="/users" activeClassName="active">
                    <i className="icon-user"></i> Users
                </Link>
            );
        }

        return dataToRender;
    },

    renderLink: function (type) {
        var dataToRender = {
            analytics: this.renderAnalytics(),
            dashboard: this.renderDashboardLink(),
            feedback: this.renderFeedbackLink(),
            tags: this.renderTagsLink(),
            users: this.renderUsersLink(),
            groups: this.renderGroupsLink(),
        };

        if (!UsersStore.isAuthenticated()) {
            dataToRender['analytics'] = null;
            dataToRender['dashboard'] = null;
            dataToRender['feedback'] = null;
            dataToRender['tags'] = null;
            dataToRender['users'] = null;
            dataToRender['groups'] = null;
        }

        return dataToRender[type];
    },
});

module.exports = AppSidebar;
