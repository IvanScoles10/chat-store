var Alert = require('react-bootstrap/lib/Alert');
var AppStore = require('../../stores/app-store');
var createReactClass = require('create-react-class');
var i18n = require('../../i18n/i18n').I18n;
var React = require('react');
var Scroll = require('react-scroll');
var Element = Scroll.Element;
var scroller = Scroll.scroller;

function getAlertState() {
    return {
        error: AppStore.getError()
    }
};

const AppAlert = createReactClass({

    getInitialState: function () {
        return getAlertState();
    },

    componentDidMount: function () {
        AppStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        AppStore.removeChangeListener(this.onChange);
    },

    componentDidUpdate: function () {
        if (this.state.error) {
            scroller.scrollTo('app-alert', {
                duration: 1000,
                delay: 100,
                offset: -10
            })
        }
    },

    onChange: function () {
        this.setState(getAlertState());
    },

    render: function () {
        var code = null;
        var dataToRender = null;

        if (this.state.error) {
            code = this.state.error.code;

            dataToRender = (
                <Element name="app-alert">
                    <Alert bsStyle="danger">
                        <h5>{i18n.t('ERROR__' + code.toUpperCase() + '__TITLE')}</h5>
                        <p>{i18n.t('ERROR__' + code.toUpperCase() + '__DESCRIPTION')}</p>
                    </Alert>
                </Element>
            );
        }

        return dataToRender;
    }
});

module.exports = AppAlert;
