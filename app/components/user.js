var _ = require('lodash');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var React = require('react');
var LocalStorageMixin = require('react-localstorage');
var ControlLabel = require('react-bootstrap/lib/ControlLabel');
var FormGroup = require('react-bootstrap/lib/FormGroup');
var FormControl = require('react-bootstrap/lib/FormControl');
var Row = require('react-bootstrap/lib/Row');

const User = createReactClass({

    displayName: 'User-v1',

    mixins: [LocalStorageMixin],

    propTypes: {
        handleUser: PropTypes.func.isRequired
    },

    getDefaultProps: function() {
        return {
            stateFilterKeys: ['user']
        };
    },

    getInitialState: function () {
        return {
            user: {}
        };
    },

    componentDidUpdate: function () {
        if (this.state.user.id === null) {
            this.setState({
                user: {
                    id: this.getGuid(),
                    name: null
                }
            });
        }
    },

    render: function () {
        return (
            <div className="user-name">
                {this.renderForm()}
            </div>
        );
    },

    renderForm: function () {
        return (
            <form>
                <FormGroup controlId="formBasicText" validationState={this.getValidationState()}>
                    <ControlLabel>User Name</ControlLabel>
                    <FormControl {...this.getInputProps()} />
                    <FormControl.Feedback />
                </FormGroup>
            </form>
        );
    },

    getValidationState: function () {
        var user = this.state.user;
        var name = _.get(user, 'name') || '';
        var length = name.length;
        var validation = null;

        if (length > 1) {
            validation = 'success';
        } else {
            validation = 'warning';
        }

        return validation;
    },

    getSubmitButtonProps: function () {
        return {
            className: '',
            type: 'submit'
        };
    },

    getInputProps: function () {
        return {
            autoComplete: false,
            name: 'text-user-name',
            onChange: this.handleUserChange,
            placeholder: 'Insert here your name',
            required: true,
            type: 'text',
            value: _.get(this.state.user, 'name') || ''
        };
    },

    handleUserChange: function(e) {
        var user = {
            id: (this.state.user.id) ? this.state.user.id : this.getGuid(),
            name: e.target.value
        };

        this.setState({
            user: user
        });

        this.props.handleUser(user);
    },

    getGuid: function () {
        return this.encript() + this.encript(true) + this.encript(true) + this.encript();
    },

    encript: function (s) {
        var p = (Math.random().toString(16) + '000000000').substr(2, 8);

        return s ? '-' + p.substr(0,4) + '-' + p.substr(4, 4) : p;
    }
});

module.exports = User;
