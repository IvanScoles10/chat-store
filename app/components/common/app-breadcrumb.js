var Breadcrumb = require('react-bootstrap/lib/Breadcrumb');
var createReactClass = require('create-react-class');
var React = require('react');

const AppBreadcrumb = createReactClass({

    render: function () {
        return (
            <div className="app-breadcrumbs">
                <Breadcrumb>
                    <Breadcrumb.Item active>Active users</Breadcrumb.Item>
                    <Breadcrumb.Item href="#">Analytics</Breadcrumb.Item>
                </Breadcrumb>
            </div>
        );
    }
});

module.exports = AppBreadcrumb;
