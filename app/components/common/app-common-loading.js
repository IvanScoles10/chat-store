var Col = require('react-bootstrap/lib/Col');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var React = require('react');

const AppCommonLoading = createReactClass({

    propTypes: {
        loading: PropTypes.bool.isRequired,
    },

    render: function () {
        return (
            <div className="app-common-loading">
                {this.renderLoading()}
            </div>
        );
    },

    renderLoading: function () {
        var dataToRender = this.props.children;

        if (this.props.loading) {
            dataToRender = (
                <div className="app-common-loading--spinner">
                    <i className="fa fa-circle-o-notch fa-lg mt-4 fa-spin"></i>
                </div>
            );
        }

        return dataToRender;
    }
});

module.exports = AppCommonLoading;
