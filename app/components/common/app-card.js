var Col = require('react-bootstrap/lib/Col');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var React = require('react');
var Row = require('react-bootstrap/lib/Row');

const AppCard = createReactClass({

    propTypes: {
        title: PropTypes.string.isRequired,
        cardActions: PropTypes.object
    },

    render: function () {
        return (
            <div className="app-card">
                <Row>
                    <Col sm={12}>
                        <div className="animated fadeIn">
                            <div className="card">
                                <div className="card-header">
                                    {this.props.title}
                                    {this.renderCardActions()}
                                </div>
                                <div className="card-block">
                                    {this.props.children}
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    },

    renderCardActions: function () {
        var dataToRender = null;

        if (this.props.cardActions) {
            dataToRender = (
                <div className="card-actions">
                    <a onClick={this.props.cardActions.callback}>
                        <small className="text-muted">{this.props.cardActions.title}</small>
                    </a>
                </div>
            );
        }

        return dataToRender;
    }
});

module.exports = AppCard;
