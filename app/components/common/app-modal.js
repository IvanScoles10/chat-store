var Button = require('react-bootstrap/lib/Button');
var Modal = require('react-bootstrap/lib/Modal');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var React = require('react');

const AppModal = createReactClass({

    propTypes: {
        action: PropTypes.shape({
            callback: PropTypes.func.isRequired,
            class: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired
        }).isRequired,
        title: PropTypes.string.isRequired,
        show: PropTypes.bool.isRequired
    },

    getInitialState: function () {
        return {
            show: false
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            show: nextProps.show
        });
    },

    render: function () {
        return (
            <div>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header>
                        <Modal.Title>{this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.props.children}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button {...this.getCloseButtonProps()}>Close</Button>
                        {this.renderButton()}
                    </Modal.Footer>
                </Modal>
            </div>
        );
    },

    renderButton: function () {
        var dataToRender = null;

        if (this.props.action) {
            return (<Button {...this.getCallbackButtonProps()}>{this.props.action.title}</Button>);
        }
        return dataToRender;
    },

    getCallbackButtonProps: function () {
        return {
            bsStyle: this.props.action.class,
            type: 'button',
            onClick: this.props.action.callback
        };
    },

    getCloseButtonProps: function () {
        return {
            onClick: this.handleClose
        };
    },

    handleClose: function () {
        this.setState({ show: false });
    }
});

module.exports = AppModal;
