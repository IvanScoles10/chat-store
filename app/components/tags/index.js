var AppActions = require('../../actions/app-actions');
var AppCard = require('../common/app-card');
var AppStore = require('../../stores/app-store');
var createReactClass = require('create-react-class');
var TagForm = require('../forms/tags');
var TagsService = require('../../services/tags-service');
var TagsActions = require('../../actions/tags-actions');
var TagsStore = require('../../stores/tags-store');
var TagsTable = require('./tags-table');
var React = require('react');

function getState() {
    return {
        tags: TagsStore.getTags(),
        loading: AppStore.getLoading('tags')
    };
};

const Apptags = createReactClass({

    getInitialState: function () {
        return getState();
    },

    componentDidMount: function () {
        TagsActions.getTags();

        AppStore.addChangeListener(this.onChange);
        TagsStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        AppStore.removeChangeListener(this.onChange);
        TagsStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getState());
    },

    render: function () {
        return (
            <div className="app-tags">
                <div className="container">
                    <div className="wrap-container">
                        <div className="animated fadeIn">
                            <AppCard title="New Tag">
                                <TagForm {...this.getFormProps()} />
                            </AppCard>
                            <AppCard title="Tags">
                                <TagsTable {...this.getTagsTableProps()} />
                            </AppCard>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    getFormProps: function () {
        return {
            handleSubmit: this.handleSubmit
        };
    },

    getTagsTableProps: function () {
        return {
            tags: this.state.tags,
            loading: this.state.loading,
            handleDelete: this.handleDelete
        };
    },

    handleDelete: function (id) {
        TagsService.delete(id, function (err, response) {
            if (!err) {

            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    },

    handleSubmit: function (data) {
        TagsService.create({
            name: data.name,
            description: data.description
        }, function (err, response) {
            if (!err) {

            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    }
});

module.exports = Apptags;
