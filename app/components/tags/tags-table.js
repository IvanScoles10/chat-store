var _ = require('lodash');
var AppCommonLoading = require('../common/app-common-loading');
var createReactClass = require('create-react-class');
var React = require('react');
var PropTypes = require('prop-types');
var Table = require('react-bootstrap/lib/Table');

const TagsTable = createReactClass({

    propTypes: {
        handleDelete: PropTypes.func.isRequired,
        tags: PropTypes.array.isRequired,
        loading: PropTypes.bool.isRequired
    },

    getInitialState: function () {
        return {
            tags: this.props.tags,
            loading: this.props.loading
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            tags: nextProps.tags,
            loading: nextProps.loading
        });
    },

    render: function () {
        return (
            <AppCommonLoading loading={this.props.loading}>
                <Table className="tag-table" responsive>
                    <thead>
                        <tr>
                            <th className="tags-table--name">Name</th>
                            <th className="tags-table--description">Description</th>
                            <th className="tags-table--view"></th>
                            <th className="tags-table--delete"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTags()}
                    </tbody>
                </Table>
            </AppCommonLoading>
        );
    },

    renderTags: function () {
        var self = this;
        var tags = [];

        _.map(this.state.tags, function (tag, key) {
            tags.push(self.renderTag(tag, key));
        });

        return tags;
    },

    renderTag: function (tag, key) {
        return (
            <tr key={key}>
                <td className="tags-table--name">
                    <div>
                        {tag.name}
                    </div>
                </td>
                <td className="tags-table--description">
                    <div>
                        {tag.description}
                    </div>
                </td>
                <td className="tags-table--view">
                    <a {...this.getHrefProps('view-tag', tag._id) }>
                        <i className="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </td>
                <td className="tags-table--delete">
                    <a {...this.getHrefProps('delete-tag', tag._id) }>
                        <i className="fa fa-remove" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        );
    },

    getHrefProps: function (type, id) {
        var hrefProps = {
            'view-tag': {
                href: '#/tags/' + id
            },
            'delete-tag': {
                'data-tag-id': id,
                onClick: this.handleDelete
            }
        };

        return hrefProps[type];
    },

    handleDelete: function (e) {
        this.props.handleDelete(e.target.parentElement.getAttribute('data-tag-id'));
    }
});

module.exports = TagsTable;
