var AppActions = require('../../actions/app-actions');
var AppStore = require('../../stores/app-store');
var AppCard = require('../common/app-card');
var createReactClass = require('create-react-class');
var React = require('react');
var UsersActions = require('../../actions/users-actions');
var UsersTable = require('./users-table');
var UsersService = require('../../services/users-service');
var UsersStore = require('../../stores/users-store');
var SignUpForm = require('../forms/sign-up');

function getState() {
    return {
        users: UsersStore.getUsers(),
        loading: AppStore.getLoading('users')
    };
};

const Users = createReactClass({

    getInitialState: function () {
        return getState();
    },

    componentDidMount: function () {
        UsersActions.getUsers();

        AppStore.addChangeListener(this.onChange);
        UsersStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        AppStore.removeChangeListener(this.onChange);
        UsersStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getState());
    },

    render: function () {
        return (
            <div className="app-users">
                <div className="container">
                    <div className="wrap-container">
                        <AppCard title="Sign Up">
                            <SignUpForm {...this.getFormProps()} />
                        </AppCard>
                        <AppCard title="Users">
                            <UsersTable {...this.getUsersTableProps()} />
                        </AppCard>
                    </div>
                </div>
            </div>
        );
    },

    getFormProps: function () {
        return {
            handleSubmit: this.handleSubmit
        };
    },

    getUsersTableProps: function () {
        return {
            handleDelete: this.handleDelete,
            loading: this.state.loading,
            users: this.state.users
        };
    },

    handleSubmit: function (data) {
        UsersService.create({
            email: data.email,
            password: data.password,
            username: data.username,
            user_metadata: {
                first_name: data.first_name,
                last_name: data.last_name,
                gender: data.gender,
                role: data.role
            }
        }, function (err, response) {
            if (!err) {
                console.log('User created')

                UsersActions.getUsers();
            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    },

    handleDelete: function (id) {
        UsersService.delete(id, function (err, response) {
            if (!err) {
                console.log('User deleted')

                UsersActions.getUsers();
            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    }
});

module.exports = Users;
