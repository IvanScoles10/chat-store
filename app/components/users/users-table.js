var _ = require('lodash');
var AppCommonLoading = require('../common/app-common-loading');
var createReactClass = require('create-react-class');
var moment = require('moment');
var React = require('react');
var UsersService = require('../../services/users-service');
var PropTypes = require('prop-types');
var Table = require('react-bootstrap/lib/Table');

const UsersTable = createReactClass({

    propTypes: {
        handleDelete: PropTypes.func.isRequired,
        users: PropTypes.array.isRequired,
        loading: PropTypes.bool.isRequired
    },

    getInitialState: function () {
        return {
            users: this.props.users,
            loading: this.props.loading
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            users: nextProps.users,
            loading: nextProps.loading
        });
    },

    render: function () {
        return (
            <AppCommonLoading loading={this.props.loading}>
                <Table className="users-table" responsive>
                    <thead>
                        <tr>
                            <th className="users-table--id">#</th>
                            <th className="users-table--email">Email</th>
                            <th className="users-table--name">Name</th>
                            <th className="users-table--gender">Gender</th>
                            <th className="users-table--role">Role</th>
                            <th className="users-table--username">Username</th>
                            <th className="users-table--last-login">Last Login</th>
                            <th className="users-table--created-at">Created at</th>
                            <th className="users-table--updated-at">Updated at</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderUsers()}
                    </tbody>
                </Table>
            </AppCommonLoading>
        );
    },

    renderUsers: function () {
        var _self = this;
        var users = [];

        _.map(this.state.users, function (user, key) {
            users.push(_self.renderUser(user, key));
        });

        return users;
    },

    renderUser: function (user, key) {
        return (
            <tr key={key}>
                <td className="users-table--user-id">
                    <div>{user.user_id}</div>
                </td>
                <td className="users-table--email">
                    <div>{user.email}</div>
                </td>
                <td className="users-table--name">
                    <div>{user.user_metadata.last_name}, {user.user_metadata.first_name}</div>
                </td>
                <td className="users-table--gender">
                    <div>{user.user_metadata.gender}</div>
                </td>
                <td className="users-table--role">
                    <div>{user.user_metadata.role}</div>
                </td>
                <td className="users-table--username">
                    <div>{user.username}</div>
                </td>
                <td className="users-table--last-login">
                    <div>{moment(user.last_login).format('YYYY-MM-DD, h:mm:ss a')}</div>
                </td>
                <td className="users-table--created-at">
                    <div>{moment(user.created_at).format('YYYY-MM-DD, h:mm:ss a')}</div>
                </td>
                <td className="users-table--updated-at">
                    <div>{moment(user.updated_at).format('YYYY-MM-DD, h:mm:ss a')}</div>
                </td>
                <td className="users-table--delete">
                    <a {...this.getHrefProps('delete-user', user.user_id) }>
                        <i className="fa fa-remove" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        );
    },

    getHrefProps: function (type, id) {
        var hrefProps = {
            'view-user': {
                href: '#/user/' + id
            },
            'delete-user': {
                'data-user-id': id,
                onClick: this.handleDelete
            }
        };

        return hrefProps[type];
    },

    handleDelete: function (e) {
        this.props.handleDelete(e.target.parentElement.getAttribute('data-user-id'));
    }
});

module.exports = UsersTable;
