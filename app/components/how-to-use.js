var createReactClass = require('create-react-class');
var React = require('react');

const HowToUse = createReactClass({

    render: function () {
        return (
            <div className="how-to-use">How To Use</div>
        );
    }
});

module.exports = HowToUse;
