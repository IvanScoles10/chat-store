var createReactClass = require('create-react-class');
var React = require('react');

const DeviceContainer = createReactClass({

    render: function () {
        return (
            <div className="device-container">
                <div className="device-mockup iphone6_plus portrait white">
                    <div className="device">
                        <div className="screen">
                            <img src="web/img/demo-screen-1.jpg" className="img-responsive" alt="" />
                        </div>
                        <div className="button"></div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = DeviceContainer;
