var createReactClass = require('create-react-class');
var React = require('react');

const HeaderContent = createReactClass({

    render: function () {
        return (
            <div className="header-content">
                <div className="header-content-inner">
                    <h1>Create your own game with trivias</h1>
                    <a href="#download" className="btn btn-outline btn-xl page-scroll">Start Now for Free!</a>
                </div>
            </div>
        );
    }
});

module.exports = HeaderContent;
