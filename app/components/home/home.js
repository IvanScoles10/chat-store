var createReactClass = require('create-react-class');
var React = require('react');
var Row = require('react-bootstrap/lib/Row');
var Col = require('react-bootstrap/lib/Col');
var HeaderContent = require('./header-content');
var DeviceContainer = require('./device-container');

var Home = createReactClass({

    render: function () {
		return (
            <div className="home">
    			<header>
    		        <div className="container">
    			        <Row>
    				    	<Col sm={7}>
    				    		<HeaderContent />
    						</Col>
    						<Col sm={5}>
                                <DeviceContainer />
    						</Col>
    			   		</Row>
    		    	</div>
            	</header>
            </div>
        );
	}
});

module.exports = Home;
