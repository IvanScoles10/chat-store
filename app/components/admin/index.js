var _ = require('lodash');
var AppCard = require('../common/app-card');
var createReactClass = require('create-react-class');
var React = require('react');
var Button = require('react-bootstrap/lib/Button');
var Form = require('react-bootstrap/lib/Form');
var FormControl = require('react-bootstrap/lib/FormControl');
var WelcomeMessageActions = require('../../actions/welcome-message-actions');
var WelcomeMessageStore = require('../../stores/welcome-message-store');

function getMessageState() {
    return {
        messages: WelcomeMessageStore.getMessages()
    };
};

const Admin = createReactClass({

    getInitialState: function () {
        return getMessageState() || [];
    },

    componentDidMount: function () {
        WelcomeMessageActions.getMessages();
        WelcomeMessageStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        WelcomeMessageStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getMessageState());
    },

    render: function () {
        return (
            <div className="app-admin">
            	<div className="container-fluid admin">
            	    <div className="animated fadeIn">
            	        <div className="row">
            	            <div className="col-sm-6">
                                <AppCard title="Welcome message">
                                    <div className="form-group">
                                         <label>Create a new welcome message</label>
                                         <div className="input-group">
                                             <FormControl {...this.getNewWelcomeMessageProps()} />
                                             <span className="input-group-btn">
                                                 <button type="button" name="newMessageForm" onClick={this.handleSubmitMessage} className="btn btn-primary">Create</button>
                                             </span>
                                         </div>
                                     </div>
                                     <div className="form-group">
                                         <label>Select a welcome message</label>
                                         <div className="input-group">
                                             <FormControl {...this.getMessageListProps()}>
                                                 {this.state.messages.map(this.renderOptions)}
                                             </FormControl>
                                             <span className="input-group-btn">
                                                 <button type="button" name="messageListForm" onClick={this.handleSubmitMessage} className="btn btn-primary app-admin--inline-btn">Save</button>
                                             </span>
                                             <span className="input-group-btn">
                                                 <button type="button" onClick={this.getDeleteButtonProps} className="btn btn-primary app-admin--inline-btn">Delete</button>
                                             </span>
                                         </div>
                                     </div>
                                </AppCard>
                            </div>
            	        </div>
                    </div>
                </div>
            </div>
        );
    },

    getNewMessageFormProps: function () {
        return {
            action: '',
            autoComplete: 'off',
            onSubmit: this.handleSubmitMessage,
            name: 'newMessageForm'
        };
    },

    getMessageListFormProps: function () {
        return {
            action: '',
            autoComplete: 'off',
            onSubmit: this.handleSubmitMessage,
            name: 'messageListForm'
        }
    },

    renderOptions: function (option) {
        //We should removed the selected value when the change on the server is Done

        return (
            <option value={option._id} key={option._id} selected={option.selected}>{option.text}</option>
        );
    },

    getNewWelcomeMessageProps: function () {
        //We should change this when react-bootstrap fixes the refs issue with the FormControls

        return {
            autoComplete: 'off',
            name: 'message',
            placeholder: 'Insert your welcome message here',
            inputRef: function (ref) {
                this.newWelcomeMessage = ref
            }.bind(this)
        };
    },

    getMessageListProps: function () {
        return {
            componentClass: 'select',
            name: 'messageList',
            inputRef: function (ref) {
                this.welcomeMessageList = ref
            }.bind(this)
        };
    },

    getSubmitButtonProps: function () {
        return {
            bsStyle: 'primary',
            type: 'submit'
        };
    },

    getDeleteButtonProps: function (e) {
        e.preventDefault();
        WelcomeMessageActions.deleteMessage(this.welcomeMessageList.value);
    },

    handleSubmitMessage: function (event) {
        event.preventDefault();
        var welcomeMessageList = [];

        if (event.target.name == 'newMessageForm') {
            WelcomeMessageActions.createMessage({
                text: this.newWelcomeMessage.value,
                selected: false
            });
        } else {
            this.state.messages.forEach(function(message){
                if (message._id == this.welcomeMessageList.value) {
                    message.selected = true;
                } else {
                    message.selected = false;
                }

                welcomeMessageList.push(message);
            }.bind(this))

            console.log('message list changed', welcomeMessageList);

            WelcomeMessageActions.setMessage(welcomeMessageList);
        }

        //WelcomeMessageActions.setMessage(this.welcomeMessage.value);
    }
});

module.exports = Admin;
