var _ = require('lodash');
var AppCommonLoading = require('../common/app-common-loading');
var createReactClass = require('create-react-class');
var React = require('react');
var PropTypes = require('prop-types');
var Table = require('react-bootstrap/lib/Table');

const GroupsTable = createReactClass({

    propTypes: {
        groups: PropTypes.array.isRequired,
        loading: PropTypes.bool.isRequired,
        handleDelete: PropTypes.func.isRequired
    },

    getInitialState: function () {
        return {
            groups: this.props.messages
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            groups: nextProps.groups
        });
    },

    render: function () {
        return (
            <AppCommonLoading loading={this.props.loading}>
                <Table className="groups-table" responsive>
                    <thead>
                        <tr>
                            <th className="groups-table--name">Name</th>
                            <th className="groups-table--view"></th>
                            <th className="groups-table--delete"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderGroups()}
                    </tbody>
                </Table>
            </AppCommonLoading>
        );
    },

    renderGroups: function () {
        var self = this;
        var groups = [];

        _.map(this.state.groups, function (group, key) {
            groups.push(self.renderGroup(group, key));
        });

        return groups;
    },

    renderGroup: function (group, key) {
        return (
            <tr key={key}>
                <td className="groups-table--name">
                    <div>
                        {group.name}
                    </div>
                </td>
                <td className="groups-table--view">
                    <a {...this.getHrefProps('view-groups', group._id)}>
                        <i className="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </td>
                <td className="group-table--delete">
                    <a {...this.getHrefProps('delete-groups', group._id)}>
                        <i className="fa fa-remove" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        );
    },

    getHrefProps: function (type, id) {
        var hrefProps = {
            'view-groups': {
                href: '#/groups/' + id
            },
            'delete-groups': {
                'data-group-id': id,
                onClick: this.handleDelete
            }
        };

        return hrefProps[type];
    },

    handleDelete: function (e) {
        this.props.handleDelete(e.target.parentElement.getAttribute('data-group-id'));
    }
});

module.exports = GroupsTable;
