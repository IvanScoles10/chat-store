var _ = require('lodash');
var AppCommonLoading = require('../common/app-common-loading');
var createReactClass = require('create-react-class');
var classNames = require('classnames');
var React = require('react');
var PropTypes = require('prop-types');
var Table = require('react-bootstrap/lib/Table');

const AppGroupUsersTable = createReactClass({

    propTypes: {
        users: PropTypes.array.isRequired,
        handleDelete: PropTypes.func.isRequired,
        loading: PropTypes.bool.isRequired,
        view: PropTypes.oneOf([
            'group'
        ]).isRequired
    },

    getInitialState: function () {
        return {
            users: this.props.users
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            users: nextProps.users
        });
    },

    render: function () {
        return (
            <AppCommonLoading loading={this.props.loading}>
                <Table {...this.getProps()}>
                    <thead>
                        <tr>
                            <th className="users-table--name">Name</th>
                            <th className="users-table--delete"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderUsers()}
                    </tbody>
                </Table>
            </AppCommonLoading>
        );
    },

    renderUsers: function () {
        var self = this;
        var users = [];

        _.map(this.state.users, function (user, key) {
            users.push(self.renderUser(user, key));
        });

        return users;
    },

    renderUser: function (user, key) {
        return (
            <tr key={key}>
                <td className="users-table--name">
                    <div>
                        {user.user_metadata.last_name}, {user.user_metadata.first_name}
                    </div>
                </td>
                <td className="users-table--delete">
                    <a {...this.getDeleteProps(user.user_id)}>
                        <i className="fa fa-remove" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        );
    },

    getProps: function () {
        return {
            className: this.getClass(),
            responsive: true
        };
    },

    getClass: function () {
        return classNames({
            'users-table': true,
            'app-group--users-table': (this.props.view === 'group')
        });
    },

    getDeleteProps: function (userId) {
        return {
            'data-user-id': userId,
            onClick: this.handleDelete
        };
    },

    handleDelete: function (e) {
        this.props.handleDelete(e.target.parentElement.getAttribute('data-user-id'));
    }
});

module.exports = AppGroupUsersTable;
