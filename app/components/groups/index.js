var AppActions = require('../../actions/app-actions');
var AppCard = require('../common/app-card');
var AppStore = require('../../stores/app-store');
var createReactClass = require('create-react-class');
var GroupForm = require('../forms/group');
var GroupsService = require('../../services/groups-service');
var GroupsActions = require('../../actions/groups-actions');
var GroupsStore = require('../../stores/groups-store');
var GroupsTable = require('./groups-table');
var React = require('react');

function getState() {
    return {
        groups: GroupsStore.getGroups(),
        loading: AppStore.getLoading('groups')
    };
};

const AppGroups = createReactClass({

    getInitialState: function () {
        return getState();
    },

    componentDidMount: function () {
        GroupsActions.getGroups();

        AppStore.addChangeListener(this.onChange);
        GroupsStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        AppStore.removeChangeListener(this.onChange);
        GroupsStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getState());
    },

    render: function () {
        return (
            <div className="app-group">
                <div className="container">
                    <div className="wrap-container">
                        <div className="animated fadeIn">
                            <AppCard title="New Group">
                                <GroupForm {...this.getFormProps()} />
                            </AppCard>
                            <AppCard title="Groups">
                                <GroupsTable {...this.getGroupsTableProps()} />
                            </AppCard>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    getFormProps: function () {
        return {
            handleSubmit: this.handleSubmit
        };
    },

    getGroupsTableProps: function () {
        return {
            groups: this.state.groups,
            loading: this.state.loading,
            handleDelete: this.handleDelete
        };
    },

    handleDelete: function (id) {
        GroupsService.delete(id, function (err, response) {
            if (!err) {

            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    },

    handleSubmit: function (data) {
        GroupsService.create({
            name: data.name,
            users: []
        }, function (err, response) {
            if (!err) {

            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    }
});

module.exports = AppGroups;
