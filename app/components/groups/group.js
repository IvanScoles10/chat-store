var _ = require('lodash');
var AppActions = require('../../actions/app-actions');
var AppCard = require('../common/app-card');
var AppStore = require('../../stores/app-store');
var Button = require('react-bootstrap/lib/Button');
var Col = require('react-bootstrap/lib/Col');
var createReactClass = require('create-react-class');
var GroupForm = require('../forms/group');
var GroupsService = require('../../services/groups-service');
var GroupsActions = require('../../actions/groups-actions');
var GroupUsersTable = require('./group-users-table');
var UsersActions = require('../../actions/users-actions')
var GroupsStore = require('../../stores/groups-store');
var UsersStore = require('../../stores/users-store');
var GroupsTable = require('./groups-table');
var React = require('react');
var Row = require('react-bootstrap/lib/Row');
var Select = require('react-select-plus');

function getState() {
    return {
        id: null,
        group: GroupsStore.getGroup(),
        loading: AppStore.getLoading('group'),
        user: {
            id: null
        },
        users: UsersStore.getUsers()
    };
};

const AppGroup = createReactClass({

    getInitialState: function () {
        return getState();
    },

    componentDidMount: function () {
        UsersActions.getUsers();
        GroupsActions.getGroup(this.props.params.id);

        AppStore.addChangeListener(this.onChange);
        GroupsStore.addChangeListener(this.onChange);
        UsersStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        AppStore.removeChangeListener(this.onChange);
        GroupsStore.removeChangeListener(this.onChange);
        UsersStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getState());
    },

    render: function () {
        var groupName = 'Group: ' + _.get(this.state.group, 'name', null);

        return(
            <div className="app-group">
                <div className="container">
                    <div className="wrap-container">
                        <div className="animated fadeIn">
                            <AppCard title={groupName}>
                                <Row>
                                    <Col md={6}>
                                        <Select {...this.getSelectProps()} />
                                    </Col>
                                    <Col md={6}>
                                        <Button {...this.getSubmitButtonProps()}>Add</Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12}>
                                        <GroupUsersTable {...this.getGroupUsersTableProps()} />
                                    </Col>
                                </Row>
                            </AppCard>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    getSelectProps: function () {
        return {
            className: 'menu-outer-top',
            id: 'users',
            name: 'users',
            value: _.get(this.state.user, 'user_id'),
            placeholder: 'Users',
            options: this.getOptions(),
            onChange: this.changeUser,
            menuContainerStyle: {
                'position': 'relative',
                'zIndex': 999
            },
            clearable: false
        }
    },

    getSubmitButtonProps: function () {
        return {
            bsStyle: 'primary',
            disabled: (_.get(this.state.user, 'user_id') === undefined),
            type: 'button',
            onClick: this.addUserToGroup
        };
    },

    getGroupUsersTableProps: function () {
        return {
            users: (this.state.group.users) ? this.state.group.users : [],
            handleDelete: this.deleteUserFromGroup,
            loading: this.state.loading,
            view: 'group'
        };
    },

    getOptions: function () {
        var users = [];

        _.map(this.state.users, function (user) {
            users.push({
                id: user.user_id,
                value: user.user_id,
                label: user.user_metadata.last_name + ', ' +
                    user.user_metadata.first_name
            });
        });

        return users;
    },

    changeUser: function (user) {
        var self = this;
        var user = _.find(this.state.users, {
            user_id: user.id
        });

        if (user) {
            this.setState({
                user: user
            });
        }
    },

    addUserToGroup: function () {
        GroupsService.updateUsers({
            id: this.props.params.id,
            user: this.state.user
        }, function(err) {
            if (!err) {

            } else {
                console.log(err);
            }
        });
    },

    deleteUserFromGroup: function (userId) {
        GroupsService.deleteUsers({
            id: this.props.params.id,
            userId: userId
        }, function(err) {
            if (!err) {

            } else {
                console.log(err);
            }
        });
    }
});

module.exports = AppGroup;
