var _ = require('lodash');
var Button = require('react-bootstrap/lib/Button');
var createReactClass = require('create-react-class');
var Col = require('react-bootstrap/lib/Col');
var ControlLabel = require('react-bootstrap/lib/ControlLabel');
var FormGroup = require('react-bootstrap/lib/FormGroup');
var FormControl = require('react-bootstrap/lib/FormControl');
var Joi = require('joi');
var PropTypes = require('prop-types');
var React = require('react');
var Row = require('react-bootstrap/lib/Row');
var strategy =  require('joi-validation-strategy');
var validation = require('react-validation-mixin');

const Chat = createReactClass({

    propTypes: {
        formData: PropTypes.shape({
            message: PropTypes.string
        }),
        handleFeedback: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired
    },

    validatorTypes: function () {
        return Joi.object().keys({
            message: Joi.string().min(2).max(300).required().label('Message')
        });
    },

    getInitialState: function () {
        return {
            chat: {
                message: undefined
            }
        };
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps === this.props) {
            return;
        }

        if (_.get(nextProps.formData, 'message')) {
            this.setState({
                chat: {
                    _id: _.get(nextProps.formData, '_id') || undefined,
                    message: _.get(nextProps.formData, 'message') || undefined
                }
            });
        }
    },

    getValidatorData: function () {
        return this.state.chat;
    },

    render: function () {
        return (
            <div className="chat-form">
                <form {...this.getFormProps()}>
                    <Row>
                        <Col md={12}>
                            <FormGroup {...this.getFormGroupProps('message')}>
                                <ControlLabel className="chat-form--label-message">
                                    Message
                                    <Button {...this.getFeedbackButtonProps()}><i className="fa fa-feed"></i></Button>
                                </ControlLabel>
                                <FormControl {...this.getInputProps('message')} />
                                {this.renderHelpText(this.props.getValidationMessages('message'))}
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <Button {...this.getSubmitButtonProps()}>Send</Button>
                        </Col>
                    </Row>
                </form>
            </div>
        );
    },

    renderHelpText: function (message) {
        return (
            <span className="form-control-feedback"><p>{message}</p></span>
        );
    },

    getFeedbackButtonProps: function () {
        return {
            type: 'button',
            bsStyle: 'primary',
            bsSize: 'small',
            className: 'chat-form--button-feedback',
            onClick: this.handleFeedback
        };
    },

    handleFeedback: function () {
        this.props.handleFeedback()
    },

    getFormProps: function () {
        return {
            action: '',
            autoComplete: 'off',
            method: 'POST',
            noValidate: true,
            onSubmit: this.handleSubmit
        };
    },

    getFormGroupProps: function(field) {
        var props = {
            controlId: field,
            className: (!this.props.isValid(field)) ? 'has-danger' : null
        };

        return props;
    },

    getInputProps: function (field) {
        var props = {
            message: {
                componentClass: 'textarea',
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'message',
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the message',
                type: 'text',
                value: this.state.chat.message
            }
        };

        return props[field];
    },

    getSubmitButtonProps: function () {
        return {
            bsStyle: 'primary',
            className: 'pull-right ',
            type: 'submit'
        };
    },

    handleInputChange: function (event) {
        var chat = _.cloneDeep(this.state.chat);
        var field = event.target.name;

        chat[field] = event.target.value;

        this.setState({
            chat: chat
        }, this.props.handleValidation(field));
    },

    handleSubmit: function (event) {
        event.preventDefault();

        const onValidate = (error) => {
            if (!error) {
                this.props.handleSubmit(this.state.chat);
                this.clear();
            }
        };

        this.props.validate(onValidate);
    },

    clear: function () {
        this.setState({
            chat: {
                message: undefined
            }
        });
    }
});

module.exports = validation(strategy)(Chat);
