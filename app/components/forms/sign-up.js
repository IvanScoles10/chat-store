var _ = require('lodash');
var Button = require('react-bootstrap/lib/Button');
var createReactClass = require('create-react-class');
var Col = require('react-bootstrap/lib/Col');
var ControlLabel = require('react-bootstrap/lib/ControlLabel');
var classNames = require('classnames');
var FormGroup = require('react-bootstrap/lib/FormGroup');
var FormControl = require('react-bootstrap/lib/FormControl');
var Joi = require('joi');
var PropTypes = require('prop-types');
var React = require('react');
var Row = require('react-bootstrap/lib/Row');
var Select = require('react-select-plus');
var strategy =  require('joi-validation-strategy');
var validation = require('react-validation-mixin');

const SignUp = createReactClass({

    propTypes: {
        handleSubmit: PropTypes.func.isRequired
    },

    validatorTypes: function () {
        return Joi.object().keys({
            first_name: Joi.string().alphanum().min(2).max(30).required().label('First Name'),
            last_name: Joi.string().alphanum().min(2).max(30).required().label('Last Name'),
            gender: Joi.string().alphanum().min(2).max(15).required().label('Gender'),
            email: Joi.string().min(2).max(30).required().regex(/^[^\s@]+@[^\s@]+\.[^\s@]+$/).label('Email'),
            username: Joi.string().alphanum().min(2).max(30).required().label('Username'),
            password: Joi.string().alphanum().min(2).max(30).required().regex(/[a-zA-Z0-9]{3,30}/).label('Password')
        });
    },

    getInitialState: function () {
        return {
            signUp: {
                first_name: undefined,
                last_name: undefined,
                gender: 'male',
                role: 'super_admin',
                username: undefined,
                email: undefined,
                password: undefined
            }
        };
    },

    getValidatorData: function () {
        return this.state.signUp;
    },

    render: function () {
        return (
            <div className="sign-up-form">
                <form {...this.getFormProps()}>
                    <Row>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('first_name')}>
                                <ControlLabel>First Name</ControlLabel>
                                <FormControl {...this.getInputProps('first_name')} />
                                {this.renderHelpText(this.props.getValidationMessages('first_name'))}
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('last_name')}>
                                <ControlLabel>Last Name</ControlLabel>
                                <FormControl {...this.getInputProps('last_name')} />
                                {this.renderHelpText(this.props.getValidationMessages('last_name'))}
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('gender')}>
                                <ControlLabel>Gender</ControlLabel>
                                <Select {...this.getInputProps('gender')} />
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('role')}>
                                <ControlLabel>Role</ControlLabel>
                                <Select {...this.getInputProps('role')} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('username')}>
                                <ControlLabel>Username</ControlLabel>
                                <FormControl {...this.getInputProps('username')} />
                                {this.renderHelpText(this.props.getValidationMessages('username'))}
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('email')}>
                                <ControlLabel>Email</ControlLabel>
                                <FormControl {...this.getInputProps('email')} />
                                {this.renderHelpText(this.props.getValidationMessages('email'))}
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('password')}>
                                <ControlLabel>Password</ControlLabel>
                                <FormControl {...this.getInputProps('password')} />
                                {this.renderHelpText(this.props.getValidationMessages('password'))}
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <Button {...this.getSubmitButtonProps()}>Send</Button>
                        </Col>
                        <Col md={6}></Col>
                    </Row>
                </form>
            </div>
        );
    },

    renderHelpText(message) {
        return (
            <span className="form-control-feedback"><p>{message}</p></span>
        );
    },

    getFormProps: function () {
        return {
            action: '',
            autoComplete: 'off',
            method: 'POST',
            noValidate: true,
            onSubmit: this.handleSubmit
        };
    },

    getFormGroupProps: function(field) {
        var props = {
            controlId: field,
            className: (!this.props.isValid(field)) ? 'has-danger' : null
        };

        return props;
    },

    getInputProps: function (field) {
        var props = {
            email: {
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'email',
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the email',
                type: 'email',
                value: this.state.signUp.email
            },
            first_name: {
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'first_name',
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the first name',
                type: 'text',
                value: this.state.signUp.first_name
            },
            gender: {
                ref: field,
                id: field,
                className: 'menu-outer-top',
                //id: message.id,
                name: 'role',
                value: this.state.signUp.gender,
                type: 'text',
                placeholder: 'Role',
                options: this.getOptions('gender'),
                onChange: this.changeOption,
                onBlur: this.props.handleValidation(field),
                menuContainerStyle: {
                    'position': 'relative',
                    'zIndex': 999
                },
                clearable: false
            },
            last_name: {
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'last_name',
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the last name',
                type: 'text',
                value: this.state.signUp.last_name
            },
            password: {
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'password',
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the password',
                type: 'password',
                value: this.state.signUp.password
            },
            role: {
                ref: field,
                id: field,
                className: 'menu-outer-top',
                //id: message.id,
                name: 'role',
                value: this.state.signUp.role,
                type: 'text',
                placeholder: 'Role',
                options: this.getOptions('role'),
                onChange: this.changeOption,
                onBlur: this.props.handleValidation(field),
                menuContainerStyle: {
                    'position': 'relative',
                    'zIndex': 999
                },
                clearable: false
            },
            username: {
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'username',
                value: this.state.signUp.username,
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the username',
                type: 'text'
            }
        };

        return props[field];
    },

    getSubmitButtonProps: function () {
        return {
            bsStyle: 'primary',
            className: 'pull-right',
            type: 'submit'
        };
    },

    getOptions: function (field) {
        var options = {
            gender: [{
                value: 'male',
                label: 'Male'
            },
            {
                value: 'female',
                label: 'Female'
            }],
            role: [{
                value: 'super_admin',
                label: 'Super admin'
            },
            {
                value: 'admin',
                label: 'Admin'
            },
            {
                value: 'cotent_management',
                label: 'Content management'
            }]
        };

        return options[field];
    },

    handleInputChange: function (event) {
        var signUp = _.cloneDeep(this.state.signUp);
        var field = event.target.name;

        signUp[field] = event.target.value;

        this.setState({
            signUp: signUp
        }, this.props.handleValidation(field));
    },

    handleSubmit: function (event) {
        event.preventDefault();

        const onValidate = (error) => {
            if (!error) {
                this.props.handleSubmit(this.state.signUp);
            }
        };

        this.props.validate(onValidate);
    }
});

module.exports = validation(strategy)(SignUp);
