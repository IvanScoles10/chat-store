var _ = require('lodash');
var Button = require('react-bootstrap/lib/Button');
var createReactClass = require('create-react-class');
var Col = require('react-bootstrap/lib/Col');
var ControlLabel = require('react-bootstrap/lib/ControlLabel');
var FormGroup = require('react-bootstrap/lib/FormGroup');
var FormControl = require('react-bootstrap/lib/FormControl');
var Joi = require('joi');
var PropTypes = require('prop-types');
var React = require('react');
var Row = require('react-bootstrap/lib/Row');
var Select = require('react-select-plus');
var strategy =  require('joi-validation-strategy');
var validation = require('react-validation-mixin');

const Feedback = createReactClass({

    propTypes: {
        handleSubmit: PropTypes.func.isRequired
    },

    validatorTypes: function () {
        var validator = {
            name: Joi.string().min(2).max(150).required().label('Name'),
            message: Joi.string().min(2).max(300).required().label('Message')
        };

        if (this.state.feedback.actions && this.state.feedback.actions.length > 0) {
            _.map(this.state.feedback.actions, function (action, index) {
                validator['action_type_' + index] = Joi.string().min(2).max(150).required().label('Action Type');
                validator['action_name_' + index] = Joi.string().min(2).max(150).required().label('Action');
                validator['action_message_' + index] = Joi.string().min(2).max(150).required().label('Action Message');
            });
        }

        return Joi.object().keys(validator);
    },

    getInitialState: function () {
        var action = {};
        var actions = [];
        var feedback = {
            name: undefined,
            message: undefined,
            actions: actions
        };

        if (feedback.actions) {
            _.map(feedback.actions, function (action, index) {
                // actions
                action['action_type_' + index] = 'internal_link';
                action['action_name_' + index] = undefined;
                action['action_message_' + index] = undefined;

                actions.push(action);
            });

            feedback.actions = actions;
        }

        return {
            feedback: feedback
        };
    },

    getValidatorData: function () {
        return this.state.feedback;
    },

    render: function () {
        return (
            <div className="feedback-form">
                <form {...this.getFormProps()}>
                    <Row>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('name')}>
                                <ControlLabel>Name</ControlLabel>
                                <FormControl {...this.getInputProps('name')} />
                                {this.renderHelpText(this.props.getValidationMessages('name'))}
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('message')}>
                                <ControlLabel>Message</ControlLabel>
                                <FormControl {...this.getInputProps('message')} />
                                {this.renderHelpText(this.props.getValidationMessages('message'))}
                            </FormGroup>
                        </Col>
                    </Row>
                    {this.renderFeedbacksActions()}
                    <Row>
                        <Col md={12}>
                            <Button {...this.getSubmitButtonProps()}>Save</Button>
                            <Button {...this.getAddButtonProps()}>Add Feedback</Button>
                        </Col>
                    </Row>
                </form>
            </div>
        );
    },

    renderFeedbacksActions: function () {
        var self = this;
        var feedbacks = [];

        _.map(this.state.feedback.actions, function (action, key) {
            feedbacks.push(self.renderFeedbacksAction(action, key));
        });

        return feedbacks;
    },

    renderFeedbacksAction: function (action, index) {
        var actionTypeKey = 'action_type_' + index;
        var actionNameKey = 'action_name_' + index;
        var actionMessageKey = 'action_message_' + index;

        return (
            <Row key={'row' + index}>
                <Col md={4} key={actionTypeKey}>
                    <FormGroup {...this.getFormGroupProps(actionTypeKey)}>
                        <ControlLabel>Action Type</ControlLabel>
                        <Select {...this.getInputProps(actionTypeKey, 'action_type')} />
                    </FormGroup>
                </Col>
                <Col md={4} key={actionNameKey}>
                    <FormGroup {...this.getFormGroupProps(actionNameKey)}>
                        <ControlLabel>Action Name</ControlLabel>
                        <FormControl {...this.getInputProps(actionNameKey, 'action_name')} />
                        {this.renderHelpText(this.props.getValidationMessages(actionNameKey))}
                    </FormGroup>
                </Col>
                <Col md={4} key={actionMessageKey}>
                    <FormGroup {...this.getFormGroupProps(actionMessageKey)}>
                        <ControlLabel>Action Message</ControlLabel>
                        <FormControl {...this.getInputProps(actionMessageKey, 'action_message')} />
                        {this.renderHelpText(this.props.getValidationMessages(actionMessageKey))}
                    </FormGroup>
                </Col>
            </Row>
        );
    },

    renderHelpText(message) {
        return (
            <span className="form-control-feedback"><p>{message}</p></span>
        );
    },

    getFormProps: function () {
        return {
            action: '',
            autoComplete: 'off',
            method: 'POST',
            noValidate: true,
            onSubmit: this.handleSubmit
        };
    },

    getFormGroupProps: function(field) {
        var props = {
            key: field + '_form-group',
            controlId: field,
            className: (!this.props.isValid(field)) ? 'has-danger' : null
        };

        return props;
    },

    getInputProps: function (field, key) {
        var self = this;
        var props = {
            name: {
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'name',
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the name',
                type: 'text',
                value: this.state.feedback.name
            },
            message: {
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'message',
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the message',
                type: 'text',
                value: this.state.feedback.message
            }
        };

        if (this.state.feedback.actions && this.state.feedback.actions.length > 0) {
            _.map(this.state.feedback.actions, function (action) {
                if (key === 'action_type') {
                    props[field] = {
                        ref: field,
                        id: field,
                        className: 'menu-outer-top',
                        name: field,
                        value: self.state.feedback[field],
                        type: 'text',
                        placeholder: 'Select here the action type',
                        options: self.getOptions('action_type', field),
                        onChange: self.handleSelectChange,
                        onBlur: self.props.handleValidation(field),
                        menuContainerStyle: {
                            'position': 'relative',
                            'zIndex': 999
                        },
                        clearable: false
                    };
                }
                if (key === 'action_name') {
                    props[field] = {
                        ref: field,
                        id: field,
                        autoComplete: 'off',
                        name: field,
                        onChange: self.handleActionChange,
                        onBlur: self.props.handleValidation(field),
                        placeholder: 'Insert here the action name',
                        type: 'text',
                        value: self.state.feedback[field]
                    };
                }
                if (key === 'action_message') {
                    props[field] = {
                        ref: field,
                        id: field,
                        autoComplete: 'off',
                        name: field,
                        onChange: self.handleActionChange,
                        onBlur: self.props.handleValidation(field),
                        placeholder: 'Insert here the action message',
                        type: 'text',
                        value: self.state.feedback[field]
                    };
                }
            });
        }

        return props[field];
    },

    getOptions: function (name, type) {
        var options = {
            'action_type': [{
                value: 'internal_link',
                label: 'Internal Link',
                name: name,
                type: type
            },
            {
                value: 'external_link',
                label: 'External Link',
                name: name,
                type: type
            }]
        };

        return options[name];
    },

    getAddButtonProps: function () {
        return {
            className: 'button-margin-right pull-right',
            onClick: this.handleAddFeedback
        };
    },

    getSubmitButtonProps: function () {
        return {
            bsStyle: 'primary',
            className: 'pull-right',
            type: 'submit'
        };
    },

    handleAddFeedback: function () {
        var feedback = _.get(this.state, 'feedback', {});
        var actions = _.get(feedback, 'actions', []);
        var action = {};
        var index = actions.length;

        // set feedback
        feedback['action_type_' + index] = 'internal_link';
        feedback['action_name_' + index] = undefined;
        feedback['action_message_' + index] = undefined;

        // actions
        action['action_type_' + index] = 'internal_link';
        action['action_name_' + index] = undefined;
        action['action_message_' + index] = undefined;

        actions.push(action);
        feedback.actions = actions;

        this.setState({
            feedback: feedback
        });
    },

    handleInputChange: function (event) {
        var feedback = _.cloneDeep(this.state.feedback);
        var field = event.target.name;

        feedback[field] = event.target.value;

        this.setState({
            feedback: feedback
        }, this.props.handleValidation(field));
    },

    handleActionChange: function (event) {
        var feedback = _.cloneDeep(this.state.feedback);
        var actions = feedback.actions;
        var name = event.target.name;
        var key = this.getIndex(name);
        var action = actions[key];

        // to create dynamic actions in the state
        action[name] = event.target.value;
        // to validate dynamic field
        feedback[name] = event.target.value;

        feedback.actions[key] = action;

        this.setState({
            feedback: feedback
        }, this.props.handleValidation(name));
    },

    handleSelectChange: function (event) {
        var feedback = _.cloneDeep(this.state.feedback);
        var actions = feedback.actions;
        var key = this.getIndex(event.type);
        var action = actions[key];

        // to create dynamic actions in the state
        action[event.type] = event.value;
        // to validate dynamic field
        feedback[event.type] = event.value;

        feedback.actions[key] = action;

        this.setState({
            feedback: feedback
        }, this.props.handleValidation(event.type));
    },

    getIndex: function (field) {
        var type = field.split('_');

        return parseInt(_.last(type));
    },

    handleSubmit: function (event) {
        event.preventDefault();

        const onValidate = (error) => {
            if (!error) {
                this.props.handleSubmit(this.state.feedback);
            }
        };

        this.props.validate(onValidate);
    }
});

module.exports = validation(strategy)(Feedback);
