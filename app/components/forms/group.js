var _ = require('lodash');
var Button = require('react-bootstrap/lib/Button');
var createReactClass = require('create-react-class');
var Col = require('react-bootstrap/lib/Col');
var ControlLabel = require('react-bootstrap/lib/ControlLabel');
var classNames = require('classnames');
var FormGroup = require('react-bootstrap/lib/FormGroup');
var FormControl = require('react-bootstrap/lib/FormControl');
var Joi = require('joi');
var PropTypes = require('prop-types');
var React = require('react');
var Row = require('react-bootstrap/lib/Row');
var strategy =  require('joi-validation-strategy');
var validation = require('react-validation-mixin');

const Group = createReactClass({

    propTypes: {
        handleSubmit: PropTypes.func.isRequired
    },

    validatorTypes: function () {
        return Joi.object().keys({
            name: Joi.string().allow('').min(2).max(45).required().label('Name')
        });
    },

    getInitialState: function () {
        return {
            group: {
                name: undefined
            }
        };
    },

    getValidatorData: function () {
        return this.state.group;
    },

    render: function () {
        return (
            <div className="group-form">
                <form {...this.getFormProps()}>
                    <Row>
                        <Col md={6}>
                            <FormGroup {...this.getFormGroupProps('name')}>
                                <ControlLabel>Name</ControlLabel>
                                <FormControl {...this.getInputProps('name')} />
                                {this.renderHelpText(this.props.getValidationMessages('name'))}
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <Button {...this.getSubmitButtonProps()}>Send</Button>
                        </Col>
                    </Row>
                </form>
            </div>
        );
    },

    renderHelpText(message) {
        return (
            <span className="form-control-feedback"><p>{message}</p></span>
        );
    },

    getFormProps: function () {
        return {
            action: '',
            autoComplete: 'off',
            method: 'POST',
            noValidate: true,
            onSubmit: this.handleSubmit
        };
    },

    getFormGroupProps: function (field) {
        var props = {
            controlId: field,
            className: (!this.props.isValid(field)) ? 'has-danger' : null
        };

        return props;
    },

    getInputProps: function (field) {
        var props = {
            name: {
                ref: field,
                id: field,
                autoComplete: 'off',
                name: 'name',
                onChange: this.handleInputChange,
                onBlur: this.props.handleValidation(field),
                placeholder: 'Insert here the name',
                type: 'text',
                value: this.state.group.name
            }
        };

        return props[field];
    },

    getSubmitButtonProps: function () {
        return {
            bsStyle: 'primary',
            className: 'pull-right',
            type: 'submit'
        };
    },

    handleInputChange: function (event) {
        var group = _.cloneDeep(this.state.group);
        var field = event.target.name;

        group[field] = event.target.value;

        this.setState({
            group: group
        }, this.props.handleValidation(field));
    },

    handleSubmit: function (event) {
        event.preventDefault();

        const onValidate = (error) => {
            if (!error) {
                this.props.handleSubmit(this.state.group);
            }
        };

        this.props.validate(onValidate);
    }
});

module.exports = validation(strategy)(Group);
