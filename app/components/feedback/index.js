var AppActions = require('../../actions/app-actions');
var AppCard = require('../common/app-card');
var AppStore = require('../../stores/app-store');
var createReactClass = require('create-react-class');
var FeedbackActions = require('../../actions/feedback-actions');
var FeedbackStore = require('../../stores/feedback-store');
var FeedbackService = require('../../services/feedback-service');
var FeedbackForm = require('../forms/feedback');
var FeedbackTable = require('./feedback-table');
var React = require('react');

function getState() {
    return {
        feedbacks: FeedbackStore.getFeedbacks(),
        loading: AppStore.getLoading('feedback')
    };
};

const AppFeedback = createReactClass({

    getInitialState: function () {
        return getState();
    },

    componentDidMount: function () {
        FeedbackActions.getFeedbacks();

        AppStore.addChangeListener(this.onChange);
        FeedbackStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        AppStore.removeChangeListener(this.onChange);
        FeedbackStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        this.setState(getState());
    },

    render: function () {
        return (
            <div className="app-feedback">
                <div className="container">
                    <div className="wrap-container">
                        <div className="animated fadeIn">
                            <AppCard title="New Feedback">
                                <FeedbackForm {...this.getFormProps()} />
                            </AppCard>
                            <AppCard title="Feedback">
                                <FeedbackTable {...this.getFeedbackTableProps()} />
                            </AppCard>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    getFormProps: function () {
        return {
            handleSubmit: this.handleSubmit
        };
    },

    getFeedbackTableProps: function () {
        return {
            feedbacks: this.state.feedbacks,
            loading: this.state.loading,
            handleDelete: this.handleDelete
        };
    },

    handleSubmit: function (feedback) {
        FeedbackService.create({
            name: feedback.name,
            message: feedback.message,
            actions: feedback.actions
        }, function (err, response) {
            if (!err) {

            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    },

    handleDelete: function (id) {
        FeedbackService.delete(id, function (err, response) {
            if (!err) {

            } else {
                AppActions.setError({ code: err.error.code });
            }
        });
    }
});

module.exports = AppFeedback;
