var _ = require('lodash');
var AppCommonLoading = require('../common/app-common-loading');
var createReactClass = require('create-react-class');
var React = require('react');
var PropTypes = require('prop-types');
var Table = require('react-bootstrap/lib/Table');

const FeedbackTable = createReactClass({

    propTypes: {
        feedbacks: PropTypes.array.isRequired,
        loading: PropTypes.bool.isRequired
    },

    getInitialState: function () {
        return {
            feedbacks: this.props.feedbacks
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            feedbacks: nextProps.feedbacks
        });
    },

    render: function () {
        return (
            <AppCommonLoading loading={this.props.loading}>
                <Table className="table-feeedback" responsive>
                    <thead>
                        <tr>
                            <th className="feedback-table--name">Name</th>
                            <th className="feedback-table--message">Message</th>
                            <th className="feedback-table--view"></th>
                            <th className="feedback-table--delete"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderFeedbacks()}
                    </tbody>
                </Table>
            </AppCommonLoading>
        );
    },

    renderFeedbacks: function () {
        var self = this;
        var feedbacks = [];

        _.map(this.state.feedbacks, function (feedback, key) {
            feedbacks.push(self.renderFeedback(feedback, key));
        });

        return feedbacks;
    },

    renderFeedback: function (feedback, key) {
        return (
            <tr key={key}>
                <td className="feedback-table--name">
                    <div>
                        {feedback.name}
                    </div>
                </td>
                <td className="feedback-table--message">
                    <div>
                        {feedback.message}
                    </div>
                </td>
                <td className="feedback-table--view">
                    <a {...this.getHrefProps('view-feedback', feedback._id)}>
                        <i className="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </td>
                <td className="feedback-table--delete">
                    <a {...this.getHrefProps('delete-feedback', feedback._id)}>
                        <i className="fa fa-remove" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        );
    },

    getHrefProps: function (type, id) {
        var hrefProps = {
            'view-feedback': {
                href: '#/feedback/' + id
            },
            'delete-feedback': {
                'data-feedback-id': id,
                onClick: this.handleDelete
            }
        };

        return hrefProps[type];
    },

    handleDelete: function (e) {
        this.props.handleDelete(e.target.parentElement.getAttribute('data-feedback-id'));
    }
});

module.exports = FeedbackTable;
