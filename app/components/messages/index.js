var createReactClass = require('create-react-class');
var Messages = require('./messages');
var MessagesActions = require('../../actions/messages-actions');
var MessagesStore = require('../../stores/messages-store');
var React = require('react');

function getState(id) {
    return {
        id: id,
        message: MessagesStore.getMessage(),
        mounted: true
    }
};

const AppMessages = createReactClass({

    getInitialState: function () {
        return getState(this.props.params.id);
    },

    componentDidMount: function () {
        MessagesActions.getMessage(this.props.params.id);

        MessagesStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        this.setState({
            mounted: false
        });

        MessagesStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        if (this.state.mounted) {
            this.setState(getState(this.props.params.id));
        }
    },

    render: function () {
        return (
            <div {...this.getProps()}>
                <div className="container">
                    <div className="wrap-container">
                        <Messages {...this.getMessagesProps()} />
                    </div>
                </div>
            </div>
        );
    },

    getProps: function () {
        return {
            className: 'app-messages'
        };
    },

    getMessagesProps: function () {
        return {
            message: this.state.message
        };
    }
});

module.exports = AppMessages;
