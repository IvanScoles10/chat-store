var _ = require('lodash');
var Button = require('react-bootstrap/lib/Button');
var classNames = require('classnames');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var React = require('react');
var moment = require('moment');

const Interactions = createReactClass({

    propTypes: {
        message: PropTypes.object.isRequired
    },

    getInitialState: function () {
        return {
            message: this.props.message
        };
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps === this.props) {
            return;
        }

        this.setState({
            message: nextProps.interactions
        });
    },

    render: function () {
        return (
            <div className="app-interactions">
                <ul className="app-interactions--interactions">
                    {this.renderInteractions()}
                </ul>
            </div>
        );
    },

    renderInteractions: function () {
        var interactions = [];
        var messages = _.get(this.props.message, 'messages', []);
        var _this = this;

        _.map(messages, function (item, index) {
            interactions.push(_this.renderBubble(item, index));
        });

        return interactions;
    },

    renderBubble: function (item, key) {
        return (
            <li key={key} {...this.getClearFixClass(item)}>
                <span {...this.getChatImgClass(item)}>
                    {this.renderImage(item)}
                </span>
                <div className="app-interactions--chat-body clearfix">
                    <div className="header">
                        {this.renderHeader(item)}
                    </div>
                    {this.renderText(item)}
                </div>
            </li>
        );
    },

    renderText: function (item) {
        var dataToRender = null;

        if (_.get(item, 'feedback.name')) {
            dataToRender = (
                <div className="app-interactions--feedback">
                    <div className="buttons pull-right">
                        {this.renderFeedbackButtons(item.feedback.actions)}
                    </div>
                    <p {...this.getFeedbackClass(item)}>{item.text}</p>
                </div>
            );
        } else if (_.get(item, 'feedback.action')) {
            dataToRender = (
                <div className="buttons pull-right">
                    <Button {...this.getFeedbackButtonsProps(item, 0)}>{item.text}</Button>
                </div>
            );
        } else {
            dataToRender = (
                <p {...this.getTextClass(item)}>{item.text}</p>
            );
        }

        return dataToRender;
    },

    renderFeedbackButtons: function (buttons) {
        var dataToRender = [];
        var _this = this;

        _.map(buttons, function (item, index) {
            dataToRender.push(_this.renderButton(item, index));
        });

        return dataToRender;
    },

    renderButton: function (item, key) {
        return (
            <Button {...this.getFeedbackButtonsProps(item, key)}>{item['action_message_' + key]}</Button>
        );
    },

    getFeedbackClass: function (item) {
        return {
            className: classNames({
                'pull-right': (item.type === 'recipient'),
                'text-right': (item.type === 'recipient')
            })
        };
    },

    getFeedbackButtonsProps: function (item, key) {
        return {
            bsSize: 'small',
            bsStyle: 'primary',
            className: classNames({
                'app-interactions--button-margin-left': (key > 0),
                'pull-right': (item.type === 'recipient')
            }),
            key: key,
            disabled: true
        };
    },

    renderImage: function (item) {
        var dataToRender = null;

        if (_.get(item, 'type') === 'sender') {
            return (
                <img src={this.getSenderImage()} alt="User Avatar" className="app-interactions--circle-avatar" />
            );
        } else {
            return (
                <img src={this.getRecipientImage()} alt="User Avatar" className="app-interactions--circle-avatar" />
            );
        }

        return dataToRender;
    },

    renderHeader: function (item) {
        var dataToRender = null;

        if (_.get(item, 'type') === 'sender') {
            dataToRender = (
                <div>
                    <strong {...this.getNameClass(item)}>{this.getSenderName()}</strong>
                    <small {...this.getTextMutedClass(item)}>
                        <span className="glyphicon glyphicon-time"></span>{moment(item.timestamp).format('LLL')}
                    </small>
                </div>
            );
        } else {
            dataToRender = (
                <div>
                    <small {...this.getTextMutedClass(item)}>
                        <span className="glyphicon glyphicon-time"></span>{moment(item.timestamp).format('LLL')}
                    </small>
                    <strong {...this.getNameClass(item)}>{this.getRecipientName()}</strong>
                </div>
            );
        }

        return dataToRender;
    },

    getChatImgClass: function (item) {
        return {
            className: classNames(
                'chat-img', {
                    'app-interactions--chat-img-left pull-left': (item.type === 'sender'),
                    'app-interactions--chat-img-right pull-right': (item.type === 'recipient')
                }
            )
        };
    },

    getTextMutedClass: function (item) {
        return {
            className: classNames(
                'text-muted', {
                    'pull-right': (item.type === 'sender')
                }
            )
        };
    },

    getTextClass: function (item) {
        return {
            className: classNames({
                'pull-right': (item.type === 'recipient')
            })
        };
    },

    getNameClass: function (item) {
        return {
            className: classNames(
                'primary-font', {
                    'pull-right': (item.type === 'recipient')
                }
            )
        };
    },

    getClearFixClass: function (item) {
        return {
            className: classNames(
                'clearfix', {
                    'left': (item.type === 'sender'),
                    'right': (item.type === 'recipient')
                }
            )
        };
    },

    getSenderName: function () {
        var sender = _.get(this.props.message, 'sender', {});
        var name = '';

        if (_.get(sender, 'last_name') && _.get(sender, 'first_name')) {
            name = _.get(sender, 'last_name') + ', ' + _.get(sender, 'first_name');
        }

        return name;
    },

    getRecipientName: function () {
        var recipient = _.get(this.props.message, 'recipient', {});
        var userMedata = _.get(recipient, 'user_metadata');
        var name = '';

        if (userMedata) {
            name = userMedata.last_name + ', ' + userMedata.first_name;
        }

        return name;
    },

    getSenderImage: function () {
        var sender = _.get(this.props.message, 'sender', {});
        var picture = _.get(sender, 'profile_pic');

        return (picture) ? picture : null;
    },

    getRecipientImage: function () {
        var recipient = _.get(this.props.message, 'recipient', {});
        var picture = _.get(recipient, 'picture');

        return (picture) ? picture : null;
    }
});

module.exports = Interactions;
