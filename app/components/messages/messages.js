var _ = require('lodash');
var AppCard = require('../common/app-card');
var AppModal = require('../common/app-modal');
var ChatForm = require('../forms/chat');
var createReactClass = require('create-react-class');
var FeedbackActions = require('../../actions/feedback-actions');
var FeedbackStore = require('../../stores/feedback-store');
var Interactions = require('./interactions');
var React = require('react');
var moment = require('moment');
var PropTypes = require('prop-types');
var Session = require('../../services/session');
var Select = require('react-select-plus');
var MessagesService = require('../../services/messages-service');

function getState(message) {
    return {
        feedback: {},
        feedbacks: FeedbackStore.getFeedbacks(),
        message: message,
        mounted: true,
        showFeedbackModal: false
    };
};

const Messages = createReactClass({

    propTypes: {
        message: PropTypes.object.isRequired
    },

    getInitialState: function () {
        return getState(this.props.message);
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps === this.props) {
            return;
        }

        var newState = _.extend(this.state, {
            message: nextProps.message,
            showFeedbackModal: false
        })

        this.setState(newState);
    },

    shouldComponentUpdate: function (nextProps, nextState) {
        return this.state !== nextState;
    },

    componentDidMount: function () {
        FeedbackActions.getFeedbacks();

        FeedbackStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        this.setState({
            mounted: false
        });

        FeedbackStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        if (this.state.mounted) {
            this.setState(getState(this.state.message));
        }
    },

    render: function () {
        var chatWithName = 'Chat with:' + this.getSenderName();

        return (
            <div {...this.getProps()}>
                <AppCard title={chatWithName}>
                    <Interactions {...this.getInteractionsProps()} />
                    <ChatForm {...this.getFormProps()} />
                    <AppModal {...this.getModalProps()}>
                        {this.renderFeedbackSelect()}
                    </AppModal>
                </AppCard>
            </div>
        );
    },

    renderFeedbackSelect: function () {
        return (
            <Select {...this.getFeedbackSelectProps()}/>
        );
    },

    getProps: function () {
        return {
            className: 'messages animated fadeIn'
        };
    },

    getInteractionsProps: function () {
        return {
            message: this.state.message
        };
    },

    getModalProps: function () {
        return {
            action: {
                class: 'primary',
                callback: this.addFeedback,
                title: 'Confirm'
            },
            show: this.state.showFeedbackModal,
            title: 'Feedback'
        };
    },

    getFeedbackSelectProps: function () {
        return {
            className: 'menu-outer-top',
            name: 'feedback',
            value: this.state.feedback._id,
            placeholder: 'Feedback',
            options: this.getFeebackOptions(),
            onChange: this.changeFeebackStatus,
            menuContainerStyle: {
                'position': 'relative',
                'zIndex': 999
            },
            clearable: false
        }
    },

    getFeebackOptions: function () {
        var feedbacks = [];

        _.map(this.state.feedbacks, function (feedback) {
            feedbacks.push({
                value: feedback._id,
                label: feedback.name
            });
        });

        return feedbacks;
    },

    getFormProps: function () {
        return {
            formData: _.pick(this.state.feedback, [
                'message'
            ]),
            handleFeedback: this.handleFeedback,
            handleSubmit: this.handleSubmitMessage
        };
    },

    getInputProps: function () {
        return {
            autoComplete: 'off',
            name: 'text-message',
            onChange: this.handleMessageChange,
            placeholder: 'Insert your message here',
            type: 'text'
        };
    },

    getSubmitButtonProps: function () {
        return {
            bsStyle: 'primary',
            type: 'submit'
        };
    },

    getSenderName: function () {
        var sender = _.get(this.state.message, 'sender', {});
        var name = '';

        if (_.get(sender, 'last_name') && _.get(sender, 'first_name')) {
            name = _.get(sender, 'last_name') + ', ' + _.get(sender, 'first_name');
        }

        return name;
    },

    getFeedback: function (id) {
        return _.find(this.state.feedbacks, { _id: id });
    },

    changeFeebackStatus: function (data) {
        this.setState({
            feedback: this.getFeedback(data.value)
        });
    },

    addFeedback: function () {
        this.setState({
            showFeedbackModal: false
        });
    },

    handleMessageChange: function (e) {
        this.setState({ text: e.target.value });
    },

    handleFeedback: function () {
        this.setState({ showFeedbackModal: true });
    },

    handleSubmitMessage: function (form) {
        var self = this;
        var user = Session.getUser();
        var sender = _.get(this.state.message, 'sender', {});
        var data = {
            id: this.state.message._id,
            recipient: _.pick(user.profile, [
                'clientID',
                'email',
                'user_metadata',
                'picture',
                'user_id',
                'nickname'
            ]),
            sender: {
                id: sender.id
            },
            message: {
                text: form.message,
                type: 'recipient',
                timestamp: moment().valueOf()
            }
        };

        if (_.get(this.state.feedback, '_id')) {
            data.message = _.extend(data.message, {
                feedback: this.state.feedback
            });
        }

        MessagesService.create(data, function (err, response) {
            if (!err) {
                self.setState({
                    feedback: {
                        message: undefined
                    }
                });
            } else {
                console.log(err);
            }
        });
    }
});

module.exports = Messages;
