var _ = require('lodash');
var createReactClass = require('create-react-class');
var React = require('react');
var Label = require('react-bootstrap/lib/Label');
var moment = require('moment');
var PropTypes = require('prop-types');
var Table = require('react-bootstrap/lib/Table');
var Select = require('react-select-plus');
var MessagesService = require('../../services/messages-service');

const DashboardTable = createReactClass({

    propTypes: {
        groups: PropTypes.array.isRequired,
        tags: PropTypes.array.isRequired,
        messages: PropTypes.array.isRequired
    },

    getInitialState: function () {
        return {
            groups: this.props.groups,
            tags: this.props.tags,
            messages: this.props.messages
        };
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            groups: nextProps.groups,
            tags: nextProps.tags,
            messages: nextProps.messages
        });
    },

    render: function () {
        return (
            <Table className="messages-table" responsive>
                <thead>
                    <tr>
                        <th className="messages-table--status">Status</th>
                        <th className="messages-table--tag">Tag</th>
                        <th className="messages-table--group">Group</th>
                        <th className="messages-table--owner">Owner</th>
                        <th className="messages-table--sender">Name</th>
                        <th className="messages-table--messages">Message</th>
                        <th className="messages-table--locale">Locale</th>
                        <th className="messages-table--date">Start Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderMessages()}
                </tbody>
            </Table>
        );
    },

    renderMessages: function () {
        var self = this;
        var messages = [];

        _.map(this.state.messages, function (message, key) {
            messages.push(self.renderMessage(message, key));
        });

        return messages;
    },

    renderMessage: function (message, key) {
        return (
            <tr key={key}>
                <td className="messages-table--status">
                    <div>
                        <Select {...this.getSelectProps('status', message, key)} />
                    </div>
                </td>
                <td className="messages-table--tag">
                    <div>
                        <Select {...this.getSelectProps('tag', message, key)} />
                    </div>
                </td>
                <td className="messages-table--group">
                    <div>
                        <Select {...this.getSelectProps('group', message, key)}/>
                    </div>
                </td>
                <td className="messages-table--owner">
                    <div>
                        <Select {...this.getSelectProps('owner', message, key)}/>
                    </div>
                </td>
                <td className="messages-table--sender">
                    <div>
                        {message.sender.first_name}, {message.sender.last_name}
                    </div>
                </td>
                <td className="messages-table--messages">
                    <div>
                        <ul>{this.renderMessageContent(message.messages)}</ul>
                    </div>
                </td>
                <td className="messages-table--lang">{message.sender.locale}</td>
                <td className="messages-table--date">{moment(message.messages[0].timestamp).format('YYYY-MM-DD, h:mm:ss a')}</td>
                <td>
                    <a {...this.getHrefProps('message', message._id)}>
                        <i className="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        );
    },

    getHrefProps: function (type, id) {
        var hrefProps = {
            message: {
                href: '#/messages/' + id
            }
        };

        return hrefProps[type];
    },

    renderMessageContent: function (messages) {
        var _this = this;
        var messagesContent = [];

        _.map(messages, function (message, key) {
            if (messagesContent.length < 2) {
                messagesContent.push(_this.renderContent(message, key));
            }
        });

        return messagesContent;
    },

    renderContent: function (message, key) {
        return (
            <li key={key}>{message.text}</li>
        );
    },

    getSelectProps: function (type, message, key) {
        var props = {
            status: {
                className: 'menu-outer-top',
                id: message._id,
                name: 'status-' + key,
                value: message.status,
                placeholder: 'Status',
                options: this.getOptions('status', message._id),
                onChange: this.changeStatus,
                menuContainerStyle: {
                    'position': 'relative',
                    'zIndex': 999
                },
                clearable: false
            },
            tag: {
                className: 'menu-outer-top',
                id: message._id,
                name: 'tag-' + key,
                value: _.get(message.tag, '_id'),
                placeholder: 'Tag',
                options: this.getOptions('tag', message._id),
                onChange: this.changeTag,
                menuContainerStyle: {
                    'position': 'relative',
                    'zIndex': 999
                },
                clearable: false
            },
            group: {
                className: 'menu-outer-top',
                id: message._id,
                name: 'group-' + key,
                value: _.get(message.group, '_id'),
                placeholder: 'Group',
                options: this.getOptions('group', message._id),
                onChange: this.changeGroup,
                menuContainerStyle: {
                    'position': 'relative',
                    'zIndex': 999
                },
                clearable: false
            },
            owner: {
                className: 'menu-outer-top',
                id: message._id,
                name: 'owner-' + key,
                value: _.get(message.owner, 'user_id'),
                placeholder: 'Owner',
                options: this.getOptions('owner', message._id),
                onChange: this.changeOwner,
                menuContainerStyle: {
                    'position': 'relative',
                    'zIndex': 999
                },
                clearable: false
            }
        };

        return props[type];
    },

    getOptions: function (type, id) {
        var options = {
            status: this.getStatuses(id),
            tag: this.getTags(id),
            group: this.getGroups(id),
            owner: this.getUsersFromGroup(id)
        };

        return options[type];
    },

    getStatuses: function (id) {
        return [{
            id: id,
            value: 'open',
            label: 'Open'
        },
        {
            id: id,
            value: 'in-progress',
            label: 'In Progress'
        },
        {
            id: id,
            value: 'closed',
            label: 'Closed'
        }];
    },

    getTags: function (id) {
        var tags = [];

        _.map(this.props.tags, function (tag) {
            tags.push({
                id: id,
                value: tag._id,
                label: tag.name
            });
        });

        return tags;
    },

    getGroups: function (id) {
        var groups = [];

        _.map(this.props.groups, function (group) {
            groups.push({
                id: id,
                value: group._id,
                label: group.name
            });
        });

        return groups;
    },

    getUsersFromGroup: function (id) {
        var users = [];
        var message = _.find(this.state.messages, { _id : id });
        var group = _.find(this.props.groups, { _id : _.get(message.group, '_id') });

        if (group) {
            _.map(group.users, function (user) {
                users.push({
                    id: id,
                    value: user.user_id,
                    label: user.user_metadata.last_name + ', ' +
                        user.user_metadata.first_name
                });
            });
        }

        return users;
    },

    changeStatus: function (data) {
        var self = this;

        if (data) {
            MessagesService.setStatus({
                id: data.id,
                status: data.value
            }, function(err) {
                if (!err) {
                    self = self.changeStatusOptions(data.id, data.value);
                } else {
                    console.log(err);
                }
            });
        }
    },

    changeStatusOptions: function (id, status) {
        var messages = _.cloneDeep(this.state.messages);
        var index = _.findIndex(this.state.messages, { _id : id });

        messages[index].status = status;

        this.setState({
            messages: messages
        });
    },

    changeTag: function (data) {
        var self = this;
        var tag;

        if (data) {
            tag = _.find(this.props.tags, { _id: data.value }) || {};

            MessagesService.setTag({
                id: data.id,
                tag: tag
            }, function(err) {
                if (!err) {
                    self = self.changeTagOptions(data.id, tag);
                } else {
                    console.log(err);
                }
            });
        }
    },

    changeTagOptions: function (id, tag) {
        var messages = _.cloneDeep(this.state.messages);
        var index = _.findIndex(this.state.messages, { _id : id });
        
        messages[index].tag = tag;

        this.setState({
            messages: messages
        });
    },

    changeGroup: function (data) {
        var self = this;
        var group = null;

        if (data) {
            group = _.find(this.props.groups, { _id: data.value }) || {};

            MessagesService.setGroup({
                id: data.id,
                group: group
            }, function(err) {
                if (!err) {
                    self = self.changeGroupOptions(data.id, group);
                } else {
                    console.log(err);
                }
            });
        }
    },

    changeGroupOptions: function (id, group) {
        var messages = _.cloneDeep(this.state.messages);
        var index = _.findIndex(this.state.messages, { _id : id });

        messages[index].group = group;
        messages[index].groupUsers = group.users;

        this.setState({
            messages: messages
        });
    },

    changeOwner: function (data) {
        var self = this;
        var message = _.find(this.state.messages, { _id: data.id });
        var owner = null;

        if (data) {
            owner = _.find(message.groupUsers, { user_id: data.value }) || {};

            MessagesService.setOwner({
                id: data.id,
                owner: owner
            }, function(err) {
                if (!err) {
                    self = self.changeOwnerOptions(data.id, owner);
                } else {
                    console.log(err);
                }
            });
        }
    },

    changeOwnerOptions: function (id, owner) {
        var messages = _.cloneDeep(this.state.messages);
        var index = _.findIndex(this.state.messages, { _id : id });

        messages[index].owner = owner;

        this.setState({
            messages: messages
        });
    }
});

module.exports = DashboardTable;
