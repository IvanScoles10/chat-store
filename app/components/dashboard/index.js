var AppActions = require('../../actions/app-actions');
var AppCard = require('../common/app-card');
var createReactClass = require('create-react-class');
var DashboardTable = require('./dashboard-table');
var TagsActions = require('../../actions/tags-actions')
var TagsStore = require('../../stores/tags-store');
var MessagesActions = require('../../actions/messages-actions');
var MessagesStore = require('../../stores/messages-store');
var React = require('react');
var GroupsActions = require('../../actions/groups-actions');
var GroupsStore = require('../../stores/groups-store');

function getState() {
    return {
        groups: GroupsStore.getGroups(),
        tags: TagsStore.getTags(),
        messages: MessagesStore.getMessages(),
        mounted: true
    }
};

const AppDashboard = createReactClass({

    getInitialState: function () {
        return getState();
    },

    componentDidMount: function () {
        GroupsActions.getGroups();
        MessagesActions.getMessages();
        TagsActions.getTags();

        GroupsStore.addChangeListener(this.onChange);
        MessagesStore.addChangeListener(this.onChange);
        TagsStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function () {
        this.setState({
            mounted: false
        });

        GroupsStore.removeChangeListener(this.onChange);
        MessagesStore.removeChangeListener(this.onChange);
        TagsStore.removeChangeListener(this.onChange);
    },

    onChange: function () {
        if (this.state.mounted) {
            this.setState(getState(this.props.params.id));
        }
    },

    render: function () {
        return (
            <div className="app-dashboard">
                <div className="container">
                    <div className="wrap-container">
                        <AppCard title="Dashboard">
                            <DashboardTable {...this.getDashboardTableProps()} />
                        </AppCard>
                    </div>
                </div>
            </div>
        );
    },

    getDashboardTableProps: function () {
        return {
            groups: this.state.groups,
            tags: this.state.tags,
            messages: this.state.messages
        };
    }
});

module.exports = AppDashboard;
