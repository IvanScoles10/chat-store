var _ = require('lodash');
var AppCard = require('../common/app-card');
var AppStore = require('../../stores/app-store');
var AnalyticsStore = require('../../stores/analytics-store');
var AnalyticsActions = require('../../actions/analytics-actions');
var createReactClass = require('create-react-class');
var Pie = require('react-chartjs-2').Pie;
var React = require('react');

function getState() {
	return {
		analytics: AnalyticsStore.getAnalytics(),
		loading: AppStore.getLoading('analytics')
	};
};

const Analytics = createReactClass({

	getInitialState: function () {
		return getState();
	},

	componentDidMount: function () {
		AnalyticsActions.getAnalytics();

		AppStore.addChangeListener(this.onChange);
		AnalyticsStore.addChangeListener(this.onChange);
	},

	componentWillUnmount: function () {
		AppStore.removeChangeListener(this.onChange);
		AnalyticsStore.removeChangeListener(this.onChange);
	},

	onChange: function () {
		this.setState(getState());
	},

	render: function () {
		return (
			<div className="app-analytics">
				<div className="container-fluid">
					<div className="wrap-container">
						<div className="card-columns cols-2">
							<AppCard title="Current Status">
								<div className="chart-wrapper">
									<Pie {...this.getCurrentStatusProps() } />
								</div>
							</AppCard>
						</div>
					</div>
				</div>
			</div>
		);
	},

	getCurrentStatusProps: function () {
		var status = {};
		var pie = {};
		var labels = [];
		var datasetsValues = {};
		var data = [];
		var datasets = [];
		var firstColor = '#FF6384';
		var secondColor = '#36A2EB'
		var thirdColor = '#FFCE56'

		if(!_.isEmpty(this.state.analytics)) {
			status = this.state.analytics.status;

			status.map(function(elem, index) {
				labels.push(elem.name + ' (' + elem.quantity + ')');
				data.push(elem.quantity);
			});

			datasetsValues.data = data;

			datasetsValues.backgroundColor = [
				firstColor,
				secondColor,
				thirdColor
			];

			datasetsValues.hoverBackgroundColor = [
				firstColor,
				secondColor,
				thirdColor
			];

			datasets.push(datasetsValues);

			pie.labels = labels;
			pie.datasets = datasets;
		}

		return {
			data: pie
		}
	}
});

module.exports = Analytics;
