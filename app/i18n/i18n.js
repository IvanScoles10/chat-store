let ReactI18nfiy = null;

ReactI18nfiy = require('react-i18nify');

const { I18n, Translate, Localize } = ReactI18nfiy;

I18n.setTranslations({
    en: {
        ERROR__FEEDBACK_EXISTS_TITLE: 'Error',
        ERROR__FEEDBACK_EXISTS_DESCRIPTION: 'The feedback already exists.',
        ERROR__GROUP_EXISTS__TITLE: 'Error',
        ERROR__GROUP_EXISTS__DESCRIPTION: 'The group already exists.',
        ERROR__TAG_EXISTS_TITLE: 'Error',
        ERROR__TAG_EXISTS_DESCRIPTION: 'The tag already exists.',
        ERROR__USER_EXISTS__TITLE: 'Error',
        ERROR__USER_EXISTS__DESCRIPTION: 'The user already exists.',
        ERROR__USERNAME_EXISTS__TITLE: 'Error',
        ERROR__USERNAME_EXISTS__DESCRIPTION: 'The username provided is in use already.'
    }
});

I18n.setLocale('en');

module.exports = ReactI18nfiy;
