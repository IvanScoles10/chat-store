var io = require('socket.io-client');

//Implement host env to use c9 and localhost
var socket = io.connect('https://chat-store-node-server.herokuapp.com');
//socket = io.connect('http://127.0.0.1:3000');
//socket = io.connect('https://facebookbot-alexis1111.c9users.io');

var FeedbackActions = require('./actions/feedback-actions');
var GroupsActions = require('./actions/groups-actions');
var TagsActions = require('./actions/tags-actions');
var MessagesActions = require('./actions/messages-actions');

socket.on('connect', function () {
    console.log('Client connected with Socket.id', socket.id);

    socket.on('client:get_messages_by_id', function (data) {
        console.log('socket:client:get_messages_by_id', data);
        MessagesActions.getMessage(data);
    });

    socket.on('client:get_messages', function () {
        console.log('socket:client:get_messages');
        MessagesActions.getMessages();
    });

    socket.on('client:get_groups', function () {
        console.log('socket:client:get_groups');
        GroupsActions.getGroups();
    });

    socket.on('client:get_tags', function () {
        console.log('socket:client:get_tags');
        TagsActions.getTags();
    });

    socket.on('client:get_group_by_id', function (data) {
        console.log('socket:client:get_group_by_id', data);
        GroupsActions.getGroup(data);
    });

    socket.on('client:get_feedback', function () {
        console.log('socket:client:get_feedback');
        FeedbackActions.getFeedbacks();
    });

    socket.on('client:get_feedback_by_id', function (data) {
        console.log('socket:client:get_feedback_by_id', data);
        FeedbackActions.getFeedback(data);
    });
});

module.exports = socket;
