var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';
var _rooms = [];

function setRooms(rooms) {
    console.log('room-store:setRooms', rooms);

    _rooms = rooms;
};

var RoomStore = assign({}, EventEmitter.prototype, {

    getRooms: function () {
         return _rooms;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
    var action = payload.action;

    switch(action.actionType) {
        case 'GET_ROOM':
             setRooms(payload.action.data);

             break;
        case 'GET_ROOMS':
             setRooms(payload.action.data);

             break;
        case 'UPDATE_ROOMS':
            setRooms(payload.action.data);

            break;
    }

    RoomStore.emitChange();

    return true;
});

module.exports = RoomStore;
