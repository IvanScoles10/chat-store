var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';
var _analytics = {};

var AnalyticsStore = assign({}, EventEmitter.prototype, {
    
    getAnalytics: function () {
        return _analytics;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
    var action = payload.action;

    switch(action.actionType) {
        case 'GET_ANALYTICS':
            _analytics = payload.action.data

            break;
    }

    AnalyticsStore.emitChange();

    return true;
});


module.exports = AnalyticsStore;