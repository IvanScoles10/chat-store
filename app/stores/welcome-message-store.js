var _ = require('lodash');
var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;
var WelcomeMessagesService = require('../services/welcome-messages-service');

var CHANGE_EVENT = 'change';
var _messages = [];

function setMessage(message) {
	console.log('welcome-message-store:setMessage', message);
	_messages.push(message)
};

function removeMessage(id) {
	_messages = _.filter(_messages, function(message) {
    	return message._id != id;
	});
}

var WelcomeMessageStore = assign({}, EventEmitter.prototype, {

	getMessages: function () {
		return _messages;
	},

	emitChange: function () {
		this.emit(CHANGE_EVENT);
	},

	addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
	var action = payload.action;
	var validResponse;

	switch(action.actionType) {
		case 'CREATE_WELCOME_MESSAGE':
			WelcomeMessagesService.create(action.data, function (err, response) {
				if (!err) {
					setMessage(_.extend({}, action.data, { _id: response.id }));
				}

				WelcomeMessageStore.emit(CHANGE_EVENT);
			});

			break;

		case 'GET_WELCOME_MESSAGE':
			_messages = action.messages;

			break;

        case 'SET_WELCOME_MESSAGE':
			WelcomeMessagesService.update(action.data, function (err, response) {
				WelcomeMessageStore.emit(CHANGE_EVENT);
			});

            break;

        case 'DELETE_WELCOME_MESSAGE':

			WelcomeMessagesService.delete(action.data, function (err, response) {
				if (!err) {
					removeMessage(action.data);
				}
			});

        	break;
    }

    WelcomeMessageStore.emitChange();

    return true;
});

module.exports = WelcomeMessageStore;
