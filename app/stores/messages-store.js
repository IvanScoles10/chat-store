var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';
var _message = {};
var _messages = [];

function setMessage(message) {
    console.log('room-store:setMessage', message);

    _message = message;
};

function setMessages(messages) {
    console.log('room-store:setMessages', messages);

    _messages = messages;
};

var MessagesStore = assign({}, EventEmitter.prototype, {

    getMessage: function () {
        return _message;
    },

    getMessages: function () {
        return _messages;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
    var action = payload.action;

    switch(action.actionType) {
        case 'GET_MESSAGE':
            setMessage(payload.action.data);

            break;
        case 'GET_MESSAGES':
            setMessages(payload.action.data);

            break;
    }

    MessagesStore.emitChange();

    return true;
});

module.exports = MessagesStore;
