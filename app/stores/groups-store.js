var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';
var _group = {};
var _groups = [];

function setGroup(group) {
    console.log('room-store:setGroup', group);

    _group = group;
};

function setGroups(groups) {
    console.log('room-store:setGroups', groups);

    _groups = groups;
};

var GroupsStore = assign({}, EventEmitter.prototype, {

    getGroup: function () {
        return _group;
    },

    getGroups: function () {
        return _groups;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
    var action = payload.action;

    switch(action.actionType) {
        case 'GET_GROUP':
            setGroup(payload.action.data);

            break;
        case 'GET_GROUPS':
            setGroups(payload.action.data);

            break;
    }

    GroupsStore.emitChange();

    return true;
});

module.exports = GroupsStore;
