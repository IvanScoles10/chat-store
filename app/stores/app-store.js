var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';
var _app = {
    loading: []
};

function setLoading(data) {
    console.log('app-store:setLoading', data);

    _app.loading[data.type] = data.value;
};

function setError(error) {
    console.log('app-store:setError', error);

    _app.error = error;
}

var AppStore = assign({}, EventEmitter.prototype, {

    getLoading: function (type) {
        return _app.loading[type] || false;
    },

    getError: function () {
        return _app.error;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
    var action = payload.action;

    switch(action.actionType) {
        case 'SET_LOADING':
             setLoading(payload.action.data);

             break;
        case 'SET_ERROR':
             setError(payload.action.data);

             break;
    }

    AppStore.emitChange();

    return true;
});

module.exports = AppStore;
