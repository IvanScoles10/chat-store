var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;
var Session = require('../services/session');

var CHANGE_EVENT = 'change';
var _user = Session.getUser() || {};
var _users = [];

function setUsers(users) {
    console.log('user-store:setUsers', users);

    _users = users;
};

function setUser(user) {
    console.log('user-store:setUser', user);

    Session.setUser(user);

    _user = user;
};

function removeUser() {
    console.log('user-store:removeUser', _user);

    _user = {};
    Session.removeUser();
}

var UsersStore = assign({}, EventEmitter.prototype, {

    getUser: function () {
        return _user;
    },

    getUsers: function () {
        return _users;
    },

    isAuthenticated: function () {
        return  Session.isAuthenticated();
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
    var action = payload.action;

    switch(action.actionType) {
        case 'GET_USER':
             setUser(payload.action.data);

             break;
        case 'GET_USERS':
            setUsers(payload.action.data);

            break;
        case 'SET_USER':
            setUser(payload.action.data);

            break;
        case 'REMOVE_USER':
            removeUser({});

            break;
    }

    UsersStore.emitChange();

    return true;
});

module.exports = UsersStore;
