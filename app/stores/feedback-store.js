var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';
var _feedback = {};
var _feedbacks = [];

function setFeedback(feedback) {
    console.log('room-store:setFeedback', feedback);

    _feedback = feedback;
};

function setFeedbacks(feedbacks) {
    console.log('room-store:setFeedbacks', feedbacks);

    _feedbacks = feedbacks;
};

var FeedbackStore = assign({}, EventEmitter.prototype, {

    getFeedback: function () {
        return _feedback;
    },

    getFeedbacks: function () {
        return _feedbacks;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
    var action = payload.action;

    switch(action.actionType) {
        case 'GET_FEEDBACK':
            setFeedback(payload.action.data);

            break;
        case 'GET_FEEDBACKS':
            setFeedbacks(payload.action.data);

            break;
    }

    FeedbackStore.emitChange();

    return true;
});

module.exports = FeedbackStore;
