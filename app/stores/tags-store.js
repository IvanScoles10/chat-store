var AppDispatcher = require('../dispatcher/app-dispatcher');
var assign = require('object-assign');
var Events = require('events');
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';
var _tag = {};
var _tags = [];

function setTag(tag) {
    console.log('room-store:setTag', tag);

    _tag = tag;
};

function setTags(tags) {
    console.log('room-store:setTags', tags);

    _tags = tags;
};

var TagsStore = assign({}, EventEmitter.prototype, {

    getTag: function () {
        return _tag;
    },

    getTags: function () {
        return _tags;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {
    var action = payload.action;

    switch (action.actionType) {
        case 'GET_TAG':
            setTag(payload.action.data);

            break;
        case 'GET_TAGS':
            setTags(payload.action.data);

            break;
    }

    TagsStore.emitChange();

    return true;
});

module.exports = TagsStore;
