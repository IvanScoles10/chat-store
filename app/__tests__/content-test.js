require('./react-dom')('<html><body></body></html>');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var chai = require('chai');
var expect = chai.expect;

describe('content component', function () {

    var Content;
    var CreateRoom;
    var JoinRoom;
    var results;
    var button;

    beforeEach(function() {
        Content = require('../content');
        CreateRoom = require('../create-room');
        JoinRoom = require('../join-room');
    });

    it('should be rendered correctly', function () {

        results = TestUtils.renderIntoDocument(<Content />);

        expect(ReactDOM.findDOMNode(results).className).to.be.equal('content');

        button = TestUtils.scryRenderedDOMComponentsWithTag(
           results, 'button'
        );

        expect(button[0].textContent).to.equal('Create room');
        expect(button[0].className).to.equal('button-create-room');
        expect(button[1].textContent).to.equal('Join room');
        expect(button[1].className).to.equal('button-join-room');
    });

    it('should show create room form when create room button is clicked', function () {

        results = TestUtils.renderIntoDocument(<Content />);

        button = TestUtils.scryRenderedDOMComponentsWithTag(
           results, 'button'
        );

        TestUtils.Simulate.click(button[0]);

        expect(results.state.createRoom).to.equal(true);
        expect(results.state.joinRoom).to.equal(false);
    });

    it('should show join room form when create room button is clicked', function () {

        results = TestUtils.renderIntoDocument(<Content />);

        button = TestUtils.scryRenderedDOMComponentsWithTag(
           results, 'button'
        );

        TestUtils.Simulate.click(button[1]);

        expect(results.state.createRoom).to.equal(false);
        expect(results.state.joinRoom).to.equal(true);
    });
});
