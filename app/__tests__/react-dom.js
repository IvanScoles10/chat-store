module.exports = function(markup) {

    var jsdom = require('jsdom').jsdom;
    var doc = jsdom(markup || '');
    var win = doc.defaultView;

    global.document = doc;
    global.window = win;
    global.navigator = {
        userAgent: 'node.js'
    };

    function propagateToGlobal (window) {
        
        for (let key in window) {
            if (!window.hasOwnProperty(key)) {
                continue
            } else {
                if (key in global) {
                    continue
                } else {
                    global[key] = window[key]
                }
            }
        }
    }
}