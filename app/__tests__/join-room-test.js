require('./react-dom')('<html><body></body></html>');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var chai = require('chai');
var expect = chai.expect;

describe('join room component', function () {

    var JoinRoom;
    var results;

    beforeEach(function() {
        JoinRoom = require('../join-room');
    });

    it('should be rendered correctly', function () {
        results = TestUtils.renderIntoDocument(<JoinRoom />);

        expect(ReactDOM.findDOMNode(results).className).to.be.equal('content--join-room');
    });
});
