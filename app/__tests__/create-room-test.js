require('./react-dom')('<html><body></body></html>');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var chai = require('chai');
var expect = chai.expect;

describe('create room component', function () {

    var CreateRoom;
    var results;

    beforeEach(function() {
        CreateRoom = require('../create-room');
    });

    it('should be rendered correctly', function () {
        results = TestUtils.renderIntoDocument(<CreateRoom />);

        expect(ReactDOM.findDOMNode(results).className).to.be.equal('content--create-room');
    });
});
