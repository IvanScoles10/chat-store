var flux = require('flux');
var Dispatcher = flux.Dispatcher;
var assign = require('object-assign');

var AppDispatcher = assign(new Dispatcher(), {

    handleViewAction: function(action) {
        this.dispatch({
            source: 'VIEW_ACTION',
            action: action
        });
    }
});

module.exports = AppDispatcher;
