var React = require('react');
var ReactDOM = require('react-dom');
var Admin = require('./components/admin/index');
var AppLayout = require('./components/app/app-layout');
var AppLoading = require('./components/app/app-loading');
var Analytics = require('./components/analytics/index');
var Contact = require('./components/contact');
var Dashboard = require('./components/dashboard/index');
var Feedback = require('./components/feedback/index');
var Group = require('./components/groups/group');
var Groups = require('./components/groups/index');
var Tags = require('./components/tags/index');
var hashHistory = require('react-router').hashHistory;
var Home = require('./components/home/home');
var IndexRoute = require('react-router').IndexRoute;
var Messages = require('./components/messages/index');
var lock = require('./services/lock');
var Router = require('react-router').Router;
var Route = require('react-router').Route;
var Users = require('./components/users/index');

var socket = require('./socket');

ReactDOM.render((
    <Router history={hashHistory}>
        <Route path="/" name="app" component={AppLayout} lock={lock}>
            <IndexRoute name="home" component={Home} />
            <Route path="/admin" name="admin" component={Admin} socket={socket} />
            <Route path="/dashboard" name="dashboard" component={Dashboard} socket={socket} />
            <Route path="/analytics" name="analytics" component={Analytics} socket={socket} />
            <Route path="/feedback" name="feedback" component={Feedback} socket={socket} />
            <Route path="/messages/:id" name="messages" component={Messages} socket={socket} />
            <Route path="/users" name="users" component={Users} socket={socket} />
            <Route path="/groups/:id" name="group" component={Group} socket={socket} />
            <Route path="/groups" name="groups" component={Groups} socket={socket} />
            <Route path="/tags" name="tags" component={Tags} socket={socket} />
        </Route>
        <AppLoading />
    </Router>
), document.getElementById('app-layout'))
